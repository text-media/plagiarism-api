<?php

// Определяем путь к основной папке, имя класса и проч.
define('TMPA_AUTOLOAD_DIRECTORY', __DIR__ . '/src');
define('TMPA_AUTOLOAD_NAMESPACE', 'TextMedia\\PlagiarismApi\\');
define('TMPA_AUTOLOAD_NS_LENGTH', strlen(TMPA_AUTOLOAD_NAMESPACE));

// Автозагрузчик для лентяев, не пользующихся composer-ом :)
spl_autoload_register(function ($class) {
    $path = ltrim($class, '\\');
    if (0 === strpos($path, TMPA_AUTOLOAD_NAMESPACE)) {
        $path = strtr(substr($path, TMPA_AUTOLOAD_NS_LENGTH), '\\', '/');
        $path = TMPA_AUTOLOAD_DIRECTORY . "/{$path}.php";
        if (is_file($path) and is_readable($path)) {
            require_once($path);
        }
    }
});
