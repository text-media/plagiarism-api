# Для разработчиков

## Обновление `master`

После внесения изменений в ветку `develop` необходимо:

 * проверить стиль оформления на соответствие [PSR-12](https://www.php-fig.org/psr/psr-12/):
   `vendor/bin/phpcs`;
 * проверить код библиотекой [phpstan](https://packagist.org/packages/phpstan/phpstan):
   `vendor/bin/phpstan analyse src tests`;
 * выполнить [тесты](https://packagist.org/packages/phpunit/phpunit):
   `vendor/bin/phpunit`;
 * выполнить сборку `phar`:
   `php makephar.php <версия>`;
 * слить ветку `develop` в `master`;
 * создать тэг новой версии (должна совпадать с указанной в пункте создания `phar`);
 * обновить версию библиотеки и кэш документации в проекте [api.proxy](https://bitbucket.org/text-media/api.proxy),
   если документация изменилась.

## Обновление `php5`

Ветка `php5` развивается отдельно - изменения в нее сливаются не из `develop`, а из `devphp5`.

Обновлять ветку при изменении документации не нужно, т.к. она (документация) исключена из ветки.

Перед публикацией новой версии ветки `php5` необходимо так же проверить синтаксис:
`find src -name "*.php" | xargs -n1 php5.6 -l`.

Установка `php5.6`:

```bash
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install php5.6-cli php5.6-curl php5.6-json php5.6-bz2
```
