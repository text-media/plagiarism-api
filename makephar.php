#!/usr/bin/env php
<?php

$ver = (string) ($argv[1] ?? null);
if (!preg_match('#^v\d+(\.\d+){2}$#', $ver)) {
    throw new Exception('Необходимо указать версию в виде "vX.Y.Z".');
}
$file = __DIR__ . '/bin/tmpa.phar';
if (file_exists($file)) {
    unlink($file);
}
$phar = new Phar($file);
$phar->setSignatureAlgorithm(Phar::MD5);
$phar->buildFromDirectory(__DIR__ . '/src/');
$phar->addFromString('autoload.php', str_replace(
    "__DIR__ . '/src'",
    "'phar://tmpa.phar'",
    (string) file_get_contents(__DIR__ . '/autoload.php')
));
$phar->setStub('<?php
    define("VERSION", "' . $ver . '");
    define("SCRIPT", __FILE__);
    Phar::mapPhar("tmpa.phar");
    require_once "phar://tmpa.phar/autoload.php";
    if (PHP_SAPI === "cli" && getenv("TMPA") === "cli") {
        TextMedia\PlagiarismApi\Console::execute();
    }
    __HALT_COMPILER();
?>');
$phar->compressFiles(Phar::BZ2);
file_put_contents(__DIR__ . '/bin/tmpa.hash', md5_file($file));
