<?php

namespace TextMedia\PlagiarismApi\Tests;

use TextMedia\PlagiarismApi\Transport\SocketTransport;
use TextMedia\PlagiarismApi\Transport\TransportInterface;

/**
 * Тест SocketTransport.
 */
final class SocketTransportTest extends TransportsTest
{
    /**
     * @return \TextMedia\PlagiarismApi\Transport\TransportInterface Тестируемый транспорт.
     */
    protected function getTransport(): TransportInterface
    {
        return new SocketTransport();
    }
}
