<?php

require_once(__DIR__ . '/../../vendor/autoload.php');

use TextMedia\PlagiarismApi\Tests\TestServer;

(new TestServer())->process();
