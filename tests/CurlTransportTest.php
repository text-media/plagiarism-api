<?php

namespace TextMedia\PlagiarismApi\Tests;

use TextMedia\PlagiarismApi\Transport\CurlTransport;
use TextMedia\PlagiarismApi\Transport\TransportInterface;

/**
 * Тест CurlTransport.
 */
final class CurlTransportTest extends TransportsTest
{
    /**
     * @return \TextMedia\PlagiarismApi\Transport\TransportInterface Тестируемый транспорт.
     */
    protected function getTransport(): TransportInterface
    {
        return new CurlTransport();
    }
}
