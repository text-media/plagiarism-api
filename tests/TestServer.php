<?php

namespace TextMedia\PlagiarismApi\Tests;

use Exception;
use TextMedia\PlagiarismApi\Client;
use TextMedia\PlagiarismApi\Exception\ServerException;

/**
 * Тестовый сервер.
 *
 * @property-read string $server IP-адрес/порт сервера.
 */
final class TestServer
{
    /** IP-адрес сервера. */
    const ADDR = '127.0.0.1';

    /** Порт сервера. */
    const PORT = 58080;

    /** @var string Метод GET/POST. */
    protected $method;

    /** @var string Запрошенный URI. */
    protected $route;

    /** @var array Запрос. */
    protected $request;

    /** @var array Ответ. */
    protected $response;

    /**
     * Геттер.
     *
     * @param string $name Имя поля.
     *
     * @return mixed
     */
    public function __get(string $name)
    {
        switch ($name) {
            case 'server':
                return sprintf('%s:%s', self::ADDR, self::PORT);
        }
    }

    /**
     * Старт сервера.
     */
    public function start()
    {
        // Пробуем запустить локальный сервер и получить его PID.
        $command = sprintf('php -S %s -t %s/web >/dev/null 2>&1 & echo $!', $this->server, __DIR__);
        exec($command, $output);
        $pid = (int) ($output[0] ?? null);
        usleep(1000000); // Подождем, пока запустится...

        // Если запустили - назначаем обработчик, который "убьет" сервер по окончании работы скрипта.
        if (!empty($pid)) {
            register_shutdown_function(function () use ($pid) {
                exec("kill -9 {$pid}");
            });
        }
    }

    /**
     * Обработка запросов.
     */
    public function process()
    {
        // Определяем параметры запроса: метод, контроллер и собственно тело запроса в виде массива.
        $this->method   = filter_input(INPUT_SERVER, 'REQUEST_METHOD');
        $this->route    = filter_input(INPUT_SERVER, 'REQUEST_URI');
        $this->response = ['code' => 200, 'type' => '', 'body' => ''];
        switch ($this->method) {
            case 'GET':
                $this->request = filter_input_array(INPUT_GET);
                break;
            case 'POST':
                $this->request = filter_input_array(INPUT_POST);
                break;
            default:
                $this->request = [];
        }

        // Перехватываем ошибки.
        set_error_handler(function ($code, $msg, $file, $line) {
            throw new Exception("Error #{$code} {$msg} in {$file}[{$line}]", 500);
        });

        try {
            // Пробуем выполнить запрос.
            $this->processRequest();
        } catch (ServerException $ex) {
            // Ошибка сервера - преобразуем ее в соответствующий массив.
            $this->response['body'] = [
                'error_code' => $ex->getCode(),
                'error_desc' => $ex->getMessage(),
            ];
        } catch (Exception $ex) {
            // Прочие ошибки: возвращаем их в HTTP-коде и теле ответа.
            $this->response['code'] = $ex->getCode();
            $this->response['body'] = $ex->getMessage();
        } finally {
            // Восстанавливаем обработчик ошибок и отдаем ответ.
            restore_error_handler();
            $this->processResponse();
        }
    }

    /**
     * Отдача ответа.
     */
    protected function processResponse()
    {
        // Приводим к строке ответ-массив.
        if (is_array($this->response['body'])) {
            $this->response['type'] = 'application/json';
            $this->response['body'] = json_encode(
                $this->response['body'],
                JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
            );
        }

        // Если ответ пуст - возвращаем в нем HTTP-код (для тестов), если обращение было не к "/".
        $size = strlen($this->response['body']);
        if ($size === 0) {
            $this->response['body'] = ($this->route !== '/') ? (string) $this->response['code'] : '';
            $this->response['type'] = 'text/plain';
            $size = strlen($this->response['body']);
        }

        // Отдаем заголовки и тело.
        http_response_code($this->response['code']);
        header("Content-Length: {$size}");
        if (!empty($type)) {
            header("Content-Type: {$this->response['type']}");
        }
        echo $this->response['body'];
        exit(0);
    }

    /**
     * Обработка запроса.
     *
     * @throws \Exception
     * @throws \TextMedia\PlagiarismApi\Exception\ServerException
     */
    protected function processRequest()
    {
        // Если запрос не к контроллерам API - отдаем только HTTP-статус, основанный на имени контроллера,
        // т.е.: / и /200 вернут код 200, /500 - 500 (и т.п.), все прочие нечисловые - 404 (не найден).
        $requestAccount = (bool) preg_match('#^/account(/.*)?$#', $this->route);
        $requestPost    = (bool) preg_match('#^/post(/.*)?$#', $this->route);
        if (!$requestAccount && !$requestPost) {
            throw new Exception(
                '',
                ($this->route === '/' || preg_match('#^/[12345]\d{2}$#', $this->route))
                    ? (int) substr($this->route, 1) ?: 200
                    : 404
            );
        }

        // К контроллерам API можно обращаться только методом POST!
        // Хотя реализованные транспорты и не поддерживают иных методов - пусть будет (задел на будущее).
        if ($this->method !== 'POST') {
            throw new Exception('', 405);
        }

        // Обращение к API - проверим API-ключ в запросе.
        if (!isset($this->request[Client::USERKEY_FIELD])) {
            throw new ServerException('', 20);
        } elseif (strlen((string) $this->request[Client::USERKEY_FIELD]) === 0) {
            throw new ServerException('', 21);
        }

        /**
         * @todo
         * Проверим существование API-ключа.
         * Переадресуем запрос методам processRequestAccount/processRequestPost.
         */
    }
}
