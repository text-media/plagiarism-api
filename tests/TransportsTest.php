<?php

namespace TextMedia\PlagiarismApi\Tests;

use PHPUnit\Framework\TestCase;
use TextMedia\PlagiarismApi\Exception\TransportException;
use TextMedia\PlagiarismApi\Transport\TransportInterface;

/**
 * Тест транспорта.
 */
abstract class TransportsTest extends TestCase
{
    /**
     * @return \TextMedia\PlagiarismApi\Transport\TransportInterface Тестируемый транспорт.
     */
    abstract protected function getTransport(): TransportInterface;

    /**
     * Тестирование ошибок транспорта.
     *
     * @param string  $url    URL, куда следует отправлять запрос.
     * @param string  $method Метод.
     * @param array   $params Параметры запроса.
     * @param integer $exCode Код исключения.
     *
     * @dataProvider badTestProvider
     */
    final public function testErrors(string $url, string $method, array $params, int $exCode)
    {
        try {
            $transport = $this->getTransport();
        } catch (TransportException $ex) {
            $this->markTestSkipped($ex->getMessage());
            return;
        }

        $this->expectException(TransportException::class);
        $this->expectExceptionCode($exCode);
        $transport->process($url, $params);
        unset($method);
    }

    /**
     * Поставщик тестов для testErrors().
     *
     * @return array См. описание testErrors().
     */
    final public function badTestProvider(): array
    {
        $validServer = sprintf('http://%s:%s', TestServer::ADDR, TestServer::PORT);
        return [
            ["{$validServer}/",    'POST', [], 3],
            ["{$validServer}/403", 'POST', [], 2],
            ["{$validServer}/404", 'POST', [], 2],
            ["{$validServer}/500", 'POST', [], 2],
            ["{$validServer}/rnd", 'POST', [], 2],
            ["htt://zzzzzzzz/rnd", 'POST', [], 6],
        ];
    }

    /**
     * Тестирование успешных запросов транспорта.
     *
     * @param string $url      URL, куда следует отправлять запрос.
     * @param string $method   Метод.
     * @param array  $params   Параметры запроса.
     * @param string $expected Какой результат должны получить.
     *
     * @dataProvider goodTestProvider
     */
    final public function testSuccess(string $url, string $method, array $params, string $expected)
    {
        try {
            $transport = $this->getTransport();
        } catch (TransportException $ex) {
            $this->markTestSkipped($ex->getMessage());
            return;
        }

        $result = $transport->process($url, $params);
        $this->assertEquals($expected, $result);
        unset($method);
    }

    /**
     * Поставщик тестов для testSuccess().
     *
     * @return array См. описание testSuccess().
     */
    final public function goodTestProvider(): array
    {
        $validServer = sprintf('http://%s:%s', TestServer::ADDR, TestServer::PORT);
        return [
            ["{$validServer}/200", 'POST', [], '200'],
        ];
    }
}
