<?php

namespace TextMedia\PlagiarismApi\Tests;

use TextMedia\PlagiarismApi\Transport\TransportInterface;

/**
 * Транспорт-заглушка, необходимый только для тестирования настроек клиента.
 */
final class MockTransport implements TransportInterface
{
    /**
     * {@inheritDoc}
     */
    public function __construct(int $timeout = self::DEFAULT_TIMEOUT)
    {
        unset($timeout);
    }

    /**
     * {@inheritDoc}
     */
    public function __toString(): string
    {
        return '';
    }

    /**
     * {@inheritDoc}
     */
    public static function __set_state(array $data): TransportInterface
    {
        unset($data);
        return new self();
    }

    /**
     * {@inheritDoc}
     */
    public function getTimeout(): int
    {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    public function setTimeout(int $timeout): TransportInterface
    {
        unset($timeout);
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function process(string $entryPoint, array $request = null): string
    {
        unset($entryPoint, $request);
        return '';
    }

    /**
     * {@inheritDoc}
     */
    public static function check(bool $secure = false)
    {
        unset($secure);
    }
}
