<?php

namespace TextMedia\PlagiarismApi\Tests;

use TextMedia\PlagiarismApi\Transport\StreamTransport;
use TextMedia\PlagiarismApi\Transport\TransportInterface;

/**
 * Тест StreamTransport.
 */
final class StreamTransportTest extends TransportsTest
{
    /**
     * @return \TextMedia\PlagiarismApi\Transport\TransportInterface Тестируемый транспорт.
     */
    protected function getTransport(): TransportInterface
    {
        return new StreamTransport();
    }
}
