<?php

namespace TextMedia\PlagiarismApi\Tests;

use PHPUnit\Framework\TestCase;
use TextMedia\PlagiarismApi\Client;
use TextMedia\PlagiarismApi\Exception\ClientException;
use TextMedia\PlagiarismApi\Transport\TransportInterface;

/**
 * Тест клиента.
 *
 * Конструктор клиента содержит так же транспорт, но его проверка - отдельный тест,
 * поэтому здесь проверяется только API-ключ пользователя.
 */
final class ClientTest extends TestCase
{
    /**
     * @return \TextMedia\PlagiarismApi\Transport\TransportInterface Транспорт-заглушка.
     */
    protected function getTransport(): TransportInterface
    {
        return new MockTransport();
    }

    /**
     * Генерирование ключа пользователя для теста.
     *
     * @param integer $type Тип (0 - пустой, -1 - неправильный, 1 - правильный).
     *
     * @return string
     */
    protected function getUserKey(int $type): string
    {
        switch ($type) {
            case 1:
                return md5(rand() . microtime(true));
            case -1:
                return (md5((string) rand()) . (string) rand());
            default:
                return '';
        }
    }

    /**
     * Тест создания клиента, когда должно будет выброшено исключение о неправильном ключе.
     *
     * @param string  $envKey  Ключ в ENV.
     * @param string  $codeKey Ключ в коде.
     * @param integer $exCode  Код исключения.
     *
     * @dataProvider badTestProvider
     */
    public function testErrors(string $envKey, string $codeKey, int $exCode)
    {
        $this->expectException(ClientException::class);
        $this->expectExceptionCode($exCode);
        Client::setDefaultUserKey($envKey);
        new Client($codeKey, $this->getTransport());
    }

    /**
     * Поставщик тестов, для которых должно будет выброшено исключение о неправильном ключе.
     *
     * @return array [Ключ в ENV, Ключ в коде, Код исключения].
     */
    public function badTestProvider(): array
    {
        $goodKey  = $this->getUserKey(1);
        $badKey   = $this->getUserKey(-1);
        $emptyKey = $this->getUserKey(0);
        return [
            [$emptyKey, $emptyKey, 3],
            [$emptyKey, $badKey,   1],
            [$badKey,   $emptyKey, 1],
            [$badKey,   $badKey,   1],
            [$goodKey,  $badKey,   1],
        ];
    }

    /**
     * Тест создания клиента, когда не должно будет выброшено исключение о неправильном ключе.
     *
     * @param string $envKey    Ключ в ENV.
     * @param string $codeKey   Ключ в коде.
     * @param string $expectKey Ключ клиента в итоге.
     *
     * @dataProvider goodTestProvider
     */
    public function testSuccess(string $envKey, string $codeKey, string $expectKey)
    {
        Client::setDefaultUserKey($envKey);
        $client = new Client($codeKey, $this->getTransport());
        $this->assertEquals($expectKey, $client->getUserKey());
    }

    /**
     * Поставщик тестов, для которых не должно будет выброшено исключение о неправильном ключе.
     *
     * @return array [Ключ в ENV, Ключ в коде, Ключ клиента в итоге].
     */
    public function goodTestProvider(): array
    {
        $goodKey  = $this->getUserKey(1);
        $goodKey2 = $this->getUserKey(1);
        $badKey   = $this->getUserKey(-1);
        $emptyKey = $this->getUserKey(0);
        return [
            [$emptyKey, $goodKey,  $goodKey],
            [$badKey,   $goodKey,  $goodKey],
            [$goodKey,  $emptyKey, $goodKey],
            [$goodKey,  $goodKey,  $goodKey],
            [$goodKey2, $goodKey,  $goodKey],
        ];
    }
}
