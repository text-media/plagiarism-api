<?php

namespace TextMedia\PlagiarismApi\Response;

/**
 * Ответ от API на запрос "получить остаток (баланс) по пакетам символов".
 */
final class PackageBalanceResponse extends AbstractResponse
{
    /**
     * {@inheritdoc}
     */
    protected function checkRawData(array $rawData): array
    {
        $rawData['size'] = $this->checkIntegerValue($rawData, 'size');
        return $rawData;
    }

    /**
     * @return integer Суммарное число доступных для использования символов во всех имеющихся пакетах.
     */
    public function getSize(): int
    {
        return $this->rawData['size'];
    }
}
