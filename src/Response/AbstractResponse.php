<?php

namespace TextMedia\PlagiarismApi\Response;

use TextMedia\PlagiarismApi\Exception\ServerException;

/**
 * Абстракция ответов от API.
 */
abstract class AbstractResponse extends AbstractRawData implements ResponseInterface
{
    /**
     * {@inheritdoc}
     *
     * @throws \TextMedia\PlagiarismApi\Exception\ServerException
     */
    final protected function prepareRawData($rawData): array
    {
        $rawData = parent::prepareRawData($rawData);
        if (!empty($rawData['error_code'])) {
            throw new ServerException(
                $this->checkStringValue($rawData, 'error_desc'),
                $this->checkIntegerValue($rawData, 'error_code') % 100,
                null,
                [],
                $rawData
            );
        }
        return $rawData;
    }
}
