<?php

namespace TextMedia\PlagiarismApi\Response\Detail;

/**
 * Список (простых и сгруппированных по составу слов) ключей в тексте.
 */
final class TextKeyDetail extends AbstractDetail
{
    /**
     * {@inheritdoc}
     */
    protected function checkRawData(array $rawData, bool $groupped = null): array
    {
        $rawData['count'] = $this->checkIntegerValue($rawData, 'count');
        $rawData['key_title'] = $this->checkStringValue($rawData, 'key_title');

        $this->setHiddenObjects([
            'sub_keys' => $groupped
                ? array_map(
                    function ($item) {
                        return new self($item, false);
                    },
                    $this->checkArrayValue($rawData, 'sub_keys', '', true)
                )
                : null,
        ]);

        return $rawData;
    }

    /**
     * @return integer Количество вхождений ключа в текст во всех формах.
     */
    public function getCount(): int
    {
        return $this->rawData['count'];
    }

    /**
     * @return string Текст ключа.
     */
    public function getTitle(): string
    {
        return $this->rawData['key_title'];
    }

    /**
     * @return \TextMedia\PlagiarismApi\Response\Detail\TextKeyDetail[]|NULL Список подключей.
     */
    public function getSubKeys()
    {
        return $this->getHiddenObject('sub_keys');
    }

    /**
     * @return boolean Является ли данный ключ сгруппированным по составу слов.
     */
    public function isGroupped(): bool
    {
        return !is_null($this->getSubKeys());
    }
}
