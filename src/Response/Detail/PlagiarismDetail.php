<?php

namespace TextMedia\PlagiarismApi\Response\Detail;

use DateTime;
use Exception;
use TextMedia\PlagiarismApi\Exception\ResponseException;
use TextMedia\PlagiarismApi\Response\Detail\PercentDetail;

/**
 * Детальная информация о результатах проверки на уникальность.
 */
final class PlagiarismDetail extends AbstractDetail
{
    /**
     * {@inheritdoc}
     */
    protected function checkRawData(array $rawData): array
    {
        static $prefix = 'result_json/';

        $rawData['unique'] = $this->checkPercentValue($rawData, 'unique', $prefix);
        $rawData['clear_text'] = isset($rawData['clear_text'])
            ? $this->checkStringValue($rawData, 'clear_text', $prefix)
            : null;
        $rawData['mixed_words'] = !empty($rawData['mixed_words'])
            ? $this->checkStrIntArray($rawData, 'mixed_words', $prefix)
            : null;
        $rawData['date_check'] = $this->checkStringValue($rawData, 'date_check', $prefix);
        try {
            new DateTime($rawData['date_check']);
        } catch (Exception $ex) {
            throw new ResponseException('', 1, $ex, ["{$prefix}date_check"], [$rawData, 'date_check']);
        }

        if (isset($rawData['urls']) && !is_array($rawData['urls'])) {
            throw new ResponseException('', 1, null, ["{$prefix}urls"], [$rawData, 'urls']);
        }

        $this->setHiddenObjects([
            'unique'     => new PercentDetail($rawData['unique'], PercentDetail::TYPE_UNIQUE),
            'clear_text' => isset($rawData['clear_text'])
                ? explode(' ', $rawData['clear_text'])
                : null,
            'urls'       => isset($rawData['urls'])
                ? array_map(function ($item) {
                    return new MatchLinkDetail($item);
                }, $rawData['urls'])
                : null,
        ]);

        return $rawData;
    }

    /**
     * @return \DateTime Дата окончания проверки текста на сервере.
     */
    public function getDateTime(): DateTime
    {
        return new DateTime($this->rawData['date_check']);
    }

    /**
     * @return \TextMedia\PlagiarismApi\Response\Detail\PercentDetail Уникальность текста в процентах
     *                                                                с точностью до 2 знаков после запятой.
     */
    public function getUniqueness(): PercentDetail
    {
        return $this->getHiddenObject('unique');
    }

    /**
     * @param boolean $cut OPTIONAL Нужно ли разрезать строку в массив (по умолчанию FALSE).
     *
     * @return string|string[]|NULL Очищенный от служебных символов и знаков препинания текст,
     *                              состоящий из слов, разделенных через пробел.
     */
    public function getClearText(bool $cut = false)
    {
        return $cut ? $this->getHiddenObject('clear_text') : $this->rawData['clear_text'];
    }

    /**
     * @return array|NULL Слова, в которых используются одновременно символы из разных алфавитов.
     */
    public function getMixedWords()
    {
        return $this->rawData['mixed_words'];
    }

    /**
     * @return \TextMedia\PlagiarismApi\Response\Detail\MatchLinkDetail[] Ссылки с процентом совпадения.
     */
    public function getMatchLinks(): array
    {
        return $this->getHiddenObject('urls');
    }
}
