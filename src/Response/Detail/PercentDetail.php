<?php

namespace TextMedia\PlagiarismApi\Response\Detail;

use TextMedia\PlagiarismApi\Common\CheckPercentTrait;
use TextMedia\PlagiarismApi\Exception\ResponseException;

/**
 * Проценты уникальности/воды/спама и т.п.
 */
final class PercentDetail
{
    use CheckPercentTrait;

    /** Тип процента "уникальность". */
    const TYPE_UNIQUE = 'unique';

    /** Тип процента "вода в тексте". */
    const TYPE_WATER = 'water';

    /** Тип процента "заспамленность". */
    const TYPE_SPAM = 'spam';

    /** Тип процента "плагиат/совпадение". */
    const TYPE_PLAGIAT = 'plagiat';

    /** Класс "плохое значение процента". */
    const CLASS_BAD = 'bad';

    /** Класс "среднее значение процента". */
    const CLASS_AVERAGE = 'average';

    /** Класс "хорошее значение процента". */
    const CLASS_GOOD = 'good';

    /** Доступные типы и их пороги. */
    const TYPES = [
        self::TYPE_UNIQUE  => [self::CLASS_BAD => 70, self::CLASS_GOOD => 90],
        self::TYPE_WATER   => [self::CLASS_BAD => 30, self::CLASS_GOOD => 15],
        self::TYPE_SPAM    => [self::CLASS_BAD => 60, self::CLASS_GOOD => 30],
        self::TYPE_PLAGIAT => [self::CLASS_BAD => 90, self::CLASS_GOOD => 20],
    ];

    /** @var string Значение процента. */
    protected $value;

    /** @var string Тип, к чему относится. */
    protected $type;

    /**
     * @param mixed  $value Собственно значение процента.
     * @param string $type  Тип, к чему относится.
     *
     * @throws \TextMedia\PlagiarismApi\Exception\ResponseException
     */
    public function __construct($value, string $type)
    {
        $checked = $this->checkPercent($value);
        if ($checked === false || !isset(self::TYPES[$type])) {
            throw new ResponseException('', 7, null, [$checked, $type], [$value, $type]);
        }

        switch ($type) {
            case self::TYPE_UNIQUE:
                $value = (string) (floor($value * 100) / 100);
                break;
            case self::TYPE_WATER:
            case self::TYPE_SPAM:
            case self::TYPE_PLAGIAT:
                $value = (string) round($value, 2);
                break;
        }

        $this->value = sprintf('%0.2f', $value);
        $this->type  = $type;
    }

    /**
     * Преобразование в строку.
     *
     * @return string
     */
    public function __toString(): string
    {
        return (string) $this->value;
    }

    /**
     * Восстановление из массива (после var_export).
     *
     * @param array $data Данные для восстановления.
     *
     * @return self
     */
    public static function __set_state(array $data)
    {
        return new self($data['value'] ?? null, $data['type'] ?? null);
    }

    /**
     * @return string Значение процента.
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return string Тип, к чему относится.
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string Класс значения процента: "bad", "good", "average".
     */
    public function getClass(): string
    {
        return ($this->isBad() ? self::CLASS_BAD : ($this->isGood() ? self::CLASS_GOOD : self::CLASS_AVERAGE));
    }

    /**
     * @return boolean Является ли данный процент хорошим значением для данного типа.
     */
    public function isGood(): bool
    {
        $ranges = self::TYPES[$this->type];
        return ($ranges[self::CLASS_BAD] < $ranges[self::CLASS_GOOD])
            ? ((int) $this->value >= $ranges[self::CLASS_GOOD])
            : ((int) $this->value <= $ranges[self::CLASS_GOOD]);
    }

    /**
     * @return boolean Является ли данный процент плохим значением для данного типа.
     */
    public function isBad(): bool
    {
        $ranges = self::TYPES[$this->type];
        return ($ranges[self::CLASS_BAD] < $ranges[self::CLASS_GOOD])
            ? ((int) $this->value < $ranges[self::CLASS_BAD])
            : ((int) $this->value > $ranges[self::CLASS_BAD]);
    }

    /**
     * @return boolean Является ли данный процент средним значением для данного типа.
     */
    public function isAverage(): bool
    {
        return !($this->isGood() || $this->isBad());
    }
}
