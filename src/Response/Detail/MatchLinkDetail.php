<?php

namespace TextMedia\PlagiarismApi\Response\Detail;

use TextMedia\PlagiarismApi\Response\Detail\PercentDetail;

/**
 * Информация по ссылке и проценту совпадения текста с ней.
 */
final class MatchLinkDetail extends AbstractDetail
{
    /** Опция для fetchDomain/getUrl "вырезать из домена 'www.'". */
    const OPTION_URL_CUTWWW = 0x01;

    /** Опция для fetchDomain/getUrl "преобразовать домен в UTF". */
    const OPTION_URL_IDNUTF = 0x02;

    /** Опция по умолчанию для fetchDomain/getUrl. */
    const OPTION_URL_DEFAULT = self::OPTION_URL_CUTWWW | self::OPTION_URL_IDNUTF;

    /**
     * {@inheritdoc}
     */
    protected function checkRawData(array $rawData): array
    {
        static $prefix = 'result_json/urls/';

        $rawData['url'] = $this->checkUrlValue($rawData, 'url', $prefix, false);
        $rawData['plagiat'] = $this->checkPercentValue($rawData, 'plagiat', $prefix);
        $rawData['words'] = !empty($rawData['words'])
            ? $this->checkStrIntArray($rawData, 'words', $prefix)
            : null;

        $this->setHiddenObjects([
            'plagiat' => new PercentDetail($rawData['plagiat'], PercentDetail::TYPE_PLAGIAT),
        ]);

        return $rawData;
    }

    /**
     * @param integer $component OPTIONAL Какую часть URL нужно получить (по умолчанию NULL).
     * @param integer $options   OPTIONAL Опции обработки частей URL (по умолчанию - см. константу OPTION_URL_DEFAULT).
     *
     * @return string|integer|NULL URL.
     */
    public function getUrl(int $component = null, int $options = self::OPTION_URL_DEFAULT)
    {
        $result = $this->rawData['url'];

        if (!empty($component)) {
            switch ($component) {
                case PHP_URL_HOST:
                    $result = self::fetchDomain($result, $options);
                    break;
                default:
                    $result = parse_url($result, $component);
            }
        }

        return $result;
    }

    /**
     * @param integer $options OPTIONAL Опции обработки домена (по умолчанию - см. константу OPTION_URL_DEFAULT).
     *
     * @return string Домен.
     */
    public function getDomain(int $options = self::OPTION_URL_DEFAULT): string
    {
        return (string) $this->getUrl(PHP_URL_HOST, $options);
    }

    /**
     * @return \TextMedia\PlagiarismApi\Response\Detail\PercentDetail Процент совпадения.
     */
    public function getMatch(): PercentDetail
    {
        return $this->getHiddenObject('plagiat');
    }

    /**
     * @return array|NULL Номера совпавших слов (из очищенного текста).
     */
    public function getWords()
    {
        return $this->rawData['words'];
    }

    /**
     * @param string  $url     URL.
     * @param integer $options OPTIONAL Опции обработки домена (по умолчанию - см. константу OPTION_URL_DEFAULT).
     *
     * @return string Домен.
     */
    public static function fetchDomain(string $url, int $options = self::OPTION_URL_DEFAULT): string
    {
        $domain = mb_strtolower((string) parse_url($url, PHP_URL_HOST));

        if ($options & self::OPTION_URL_CUTWWW) {
            $domain = (string) preg_replace('#^www\.#u', '', $domain);
        }

        if ($options & self::OPTION_URL_IDNUTF) {
            $domain = idn_to_utf8($domain, IDNA_DEFAULT, INTL_IDNA_VARIANT_UTS46);
        } else {
            $domain = idn_to_ascii($domain, IDNA_DEFAULT, INTL_IDNA_VARIANT_UTS46);
        }

        return (string) $domain;
    }
}
