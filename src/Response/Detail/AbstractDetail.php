<?php

namespace TextMedia\PlagiarismApi\Response\Detail;

use TextMedia\PlagiarismApi\Response\AbstractRawData;

/**
 * Абстракция для детальной информации о проверке.
 */
abstract class AbstractDetail extends AbstractRawData
{
}
