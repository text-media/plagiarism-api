<?php

namespace TextMedia\PlagiarismApi\Response\Detail;

use TextMedia\PlagiarismApi\Response\Detail\PercentDetail;

/**
 * Детальная информация о результатах SEO-анализа.
 */
final class SeoAnalyzeDetail extends AbstractDetail
{
    /**
     * {@inheritdoc}
     */
    protected function checkRawData(array $rawData): array
    {
        static $prefix = 'seo_check/';

        $this->checkIntegerRange($rawData, 'count_chars_without_space', 'count_chars_with_space', $prefix);
        $rawData['count_words'] = $this->checkIntegerValue($rawData, 'count_words', $prefix);
        $rawData['water_percent'] = $this->checkPercentValue($rawData, 'water_percent', $prefix);
        $rawData['spam_percent'] = $this->checkPercentValue($rawData, 'spam_percent', $prefix);

        $objects = [
            'water_percent' => new PercentDetail($rawData['water_percent'], PercentDetail::TYPE_WATER),
            'spam_percent'  => new PercentDetail($rawData['spam_percent'], PercentDetail::TYPE_SPAM),
        ];
        foreach (['list_keys', 'list_keys_group'] as $grp => $key) {
            $list = $this->checkArrayValue($rawData, $key, $prefix, true);
            foreach ($list as $item) {
                $objects['list_keys'][$grp][] = new TextKeyDetail($item, (bool) $grp);
            }
        }
        $this->setHiddenObjects($objects);

        return $rawData;
    }

    /**
     * @param boolean $withSpaces Число символов необходимо с пробелами или без?
     *
     * @return integer Число символов.
     */
    public function getCountChars(bool $withSpaces): int
    {
        return $this->rawData[$withSpaces ? 'count_chars_with_space' : 'count_chars_without_space'];
    }

    /**
     * @return integer Число слов.
     */
    public function getCountWords(): int
    {
        return $this->rawData['count_words'];
    }

    /**
     * @return \TextMedia\PlagiarismApi\Response\Detail\PercentDetail Процент "воды" в тексте.
     */
    public function getWaterPercent(): PercentDetail
    {
        return $this->getHiddenObject('water_percent');
    }

    /**
     * @return \TextMedia\PlagiarismApi\Response\Detail\PercentDetail Процент заспамленности.
     */
    public function getSpamPercent(): PercentDetail
    {
        return $this->getHiddenObject('spam_percent');
    }

    /**
     * @param boolean $groupped Нужно ли группировать ключи по составу слов.
     *
     * @return \TextMedia\PlagiarismApi\Response\Detail\TextKeyDetail[] Список ключей в тексте
     *                                                                  (простых или сгруппированных по составу слов).
     */
    public function getListKeys(bool $groupped): array
    {
        return $this->getHiddenObject('list_keys')[(int) $groupped];
    }
}
