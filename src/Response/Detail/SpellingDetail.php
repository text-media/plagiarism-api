<?php

namespace TextMedia\PlagiarismApi\Response\Detail;

use TextMedia\PlagiarismApi\Exception\ResponseException;

/**
 * Детальная информация о результатах проверки орфографии.
 */
final class SpellingDetail extends AbstractDetail
{
    /**
     * {@inheritdoc}
     */
    protected function checkRawData(array $rawData): array
    {
        static $prefix = 'spell_check/';

        $this->checkStringValue($rawData, 'error_type', $prefix);
        $this->checkStringValue($rawData, 'error_text', $prefix);
        $this->checkStringValue($rawData, 'reason', $prefix);
        $this->checkIntegerRange($rawData, 'start', 'end', $prefix);

        $params = ['spell_check/replacements'];
        $debug  = [$rawData, 'replacements'];
        if (empty($rawData['replacements'])) {
            $rawData['replacements'] = [];
        } elseif (!is_array($rawData['replacements'])) {
            throw new ResponseException('', 1, null, $params, $debug);
        } else {
            $rawData['replacements'] = array_filter(array_map(function ($replacement): string {
                return trim((string) $replacement);
            }, $rawData['replacements']));
        }

        return $rawData;
    }

    /**
     * @return string Тип ошибки ('Орфография', 'Пунктуация' и т.д.).
     */
    public function getType(): string
    {
        return $this->rawData['error_type'];
    }

    /**
     * @return string Детальное описание ошибки.
     */
    public function getDescription(): string
    {
        return $this->rawData['reason'];
    }

    /**
     * @return string Текст фрагмента, в котором обнаружилась ошибка.
     */
    public function getText(): string
    {
        return $this->rawData['error_text'];
    }

    /**
     * @return string[] Предлагаемые варианты замены.
     */
    public function getReplacements(): array
    {
        return $this->rawData['replacements'];
    }

    /**
     * @return integer Начальная позиция фрагмента, в котором найдена ошибка.
     */
    public function getTextStart(): int
    {
        return $this->rawData['start'];
    }

    /**
     * @return integer Конечная позиция фрагмента, в котором найдена ошибка.
     */
    public function getTextEnd(): int
    {
        return $this->rawData['end'];
    }
}
