<?php

namespace TextMedia\PlagiarismApi\Response;

/**
 * Ответ от API на запрос "отправить текст на проверку уникальности".
 */
final class CheckTextResponse extends AbstractResponse
{
    /**
     * {@inheritdoc}
     */
    protected function checkRawData(array $rawData): array
    {
        $rawData['text_uid'] = $this->checkUidValue($rawData, 'text_uid');
        return $rawData;
    }

    /**
     * @return string Уникальный идентификатор текста.
     */
    public function getTextUid(): string
    {
        return $this->rawData['text_uid'];
    }
}
