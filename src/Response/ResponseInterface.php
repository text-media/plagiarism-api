<?php

namespace TextMedia\PlagiarismApi\Response;

/**
 * Интерфейс ответов от API.
 */
interface ResponseInterface
{
    /**
     * Конструктор.
     *
     * @param mixed $rawData Данные ответа (массив или строка).
     *
     * @throws \TextMedia\PlagiarismApi\Exception\ResponseException
     */
    public function __construct($rawData);

    /**
     * Преобразование в строку.
     *
     * @return string
     */
    public function __toString(): string;

    /**
     * Восстановление из массива (после var_export).
     *
     * @param array $data Данные для восстановления.
     *
     * @return self
     */
    public static function __set_state(array $data);

    /**
     * Получение данных ответа.
     *
     * @return array
     */
    public function getRawData(): array;
}
