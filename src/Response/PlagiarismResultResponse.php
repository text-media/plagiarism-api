<?php

namespace TextMedia\PlagiarismApi\Response;

use TextMedia\PlagiarismApi\Exception\ResponseException;
use TextMedia\PlagiarismApi\Response\Detail\MatchLinkDetail;
use TextMedia\PlagiarismApi\Response\Detail\PercentDetail;
use TextMedia\PlagiarismApi\Response\Detail\PlagiarismDetail;
use TextMedia\PlagiarismApi\Response\Detail\SeoAnalyzeDetail;
use TextMedia\PlagiarismApi\Response\Detail\SpellingDetail;

/**
 * Ответ от API на запрос "получить результат проверки текста на уникальность".
 */
final class PlagiarismResultResponse extends AbstractResponse
{
    /**
     * {@inheritdoc}
     *
     * @param boolean $callback OPTIONAL Данные пришли в Callback (по умолчанию - "FALSE").
     */
    protected function checkRawData(array $rawData, $callback = false): array
    {
        $rawData['text_unique'] = $this->checkPercentValue($rawData, 'text_unique');
        if ($callback) {
            $rawData['json_result'] = $this->checkJsonStringValue($rawData, 'json_result');
            $rawData['uid']         = $this->checkUidValue($rawData, 'uid');
        } else {
            $rawData['result_json'] = $this->checkJsonStringValue($rawData, 'result_json');
        }

        foreach (['spell_check', 'seo_check'] as $name) {
            if (!empty($rawData[$name])) {
                $rawData[$name] = $this->checkJsonStringValue($rawData, $name);
            }
        }

        $this->setHiddenObjects([
            'text_unique' => new PercentDetail($rawData['text_unique'], PercentDetail::TYPE_UNIQUE),
            'result_json' => new PlagiarismDetail($rawData[$callback ? 'json_result' : 'result_json']),
            'seo_check'   => !empty($rawData['seo_check']) ? new SeoAnalyzeDetail($rawData['seo_check']) : null,
            'spell_check' => !empty($rawData['spell_check'])
                ? array_map(
                    function ($item) {
                        return new SpellingDetail($item);
                    },
                    $this->parseJson($rawData['spell_check'], true)
                )
                : null,
        ]);

        return $rawData;
    }

    /**
     * @return string|NULL Уникальный идентификатор текста.
     */
    public function getTextUid()
    {
        return ($this->rawData['text_uid'] ?? null);
    }

    /**
     * @return \TextMedia\PlagiarismApi\Response\Detail\PercentDetail Уникальность текста в процентах
     *                                                                с точностью до 2 знаков после запятой.
     */
    public function getUniqueness(): PercentDetail
    {
        return $this->getHiddenObject('text_unique');
    }

    /**
     * @return \TextMedia\PlagiarismApi\Response\Detail\PlagiarismDetail Дополнительная информация о результатах
     *                                                                   проверки на уникальность.
     */
    public function getPlagiarismDetail(): PlagiarismDetail
    {
        return $this->getHiddenObject('result_json');
    }

    /**
     * @return \TextMedia\PlagiarismApi\Response\Detail\SpellingDetail[]|NULL Дополнительная информация о результатах
     *                                                                        проверки на правописание.
     */
    public function getSpellingDetail()
    {
        return $this->getHiddenObject('spell_check');
    }

    /**
     * @return \TextMedia\PlagiarismApi\Response\Detail\SeoAnalyzeDetail|NULL Дополнительная информация о результатах
     *                                                                        проверки на SEO-анализ.
     */
    public function getSeoAnalyzeDetail()
    {
        return $this->getHiddenObject('seo_check');
    }

    /**
     * @return boolean Завершена ли проверка офрфографии?
     */
    public function isSpellingComplete(): bool
    {
        return !is_null($this->getSpellingDetail());
    }

    /**
     * @return boolean Завершен SEO-анализ?
     */
    public function isSeoAnalyzeComplete(): bool
    {
        return !is_null($this->getSeoAnalyzeDetail());
    }

    /**
     * @param array $excludings Список доменов/URL для исключения из проверки.
     *
     * @throws \TextMedia\PlagiarismApi\Exception\ResponseException
     *
     * @return \TextMedia\PlagiarismApi\Response\Detail\PercentDetail
     */
    public function getResultExcluding(array $excludings): PercentDetail
    {
        $domains = $urls = [];
        foreach ($excludings as $excluding) {
            if (filter_var($excluding, FILTER_VALIDATE_URL)) {
                array_push($urls, $excluding);
            } elseif (
                filter_var($excluding, FILTER_VALIDATE_DOMAIN)
                || filter_var($excluding, FILTER_VALIDATE_IP)
            ) {
                array_push($domains, MatchLinkDetail::fetchDomain("http://{$excluding}"));
            } else {
                throw new ResponseException('', 8, null, [], [$excluding]);
            }
        }

        $links = [];
        foreach ($this->getPlagiarismDetail()->getMatchLinks() as $link) {
            $domain = $link->getDomain();
            $url    = $link->getUrl();
            if (!in_array($url, $urls) && !in_array($domain, $domains)) {
                array_push($links, $link);
            }
        }

        $textWords  = $this->getPlagiarismDetail()->getClearText(true);
        $matchWords = [];
        foreach ($links as $link) {
            $matchWords = array_unique(array_merge($matchWords, $link->getWords() ?: []));
        }

        $textSize  = mb_strlen((string) implode('', is_array($textWords) ? $textWords : []));
        $matchSize = array_sum(array_map(function (int $wordNum) use ($textWords): int {
            return mb_strlen((string) $textWords[$wordNum]);
        }, $matchWords));

        return new PercentDetail(
            max(0, 100 - round(($matchSize / $textSize) * 100, 2)),
            PercentDetail::TYPE_UNIQUE
        );
    }
}
