<?php

namespace TextMedia\PlagiarismApi\Response;

use TextMedia\PlagiarismApi\Common\CheckPercentTrait;
use TextMedia\PlagiarismApi\Common\CheckUidTrait;
use TextMedia\PlagiarismApi\Common\CheckUrlTrait;
use TextMedia\PlagiarismApi\Common\RawDataTrait;
use TextMedia\PlagiarismApi\Exception\ResponseException;

/**
 * Общие методы для ответов и их "сложных" частей.
 */
abstract class AbstractRawData
{
    use CheckPercentTrait;
    use CheckUidTrait;
    use CheckUrlTrait;
    use RawDataTrait;

    /** @var array Скрытые объекты. */
    protected static $objects = [];

    /**
     * Конструктор.
     *
     * @param string|array $rawData Данные, полученные от API (JSON-строка или массив).
     *
     * @throws \TextMedia\PlagiarismApi\Exception\ResponseException
     */
    final public function __construct($rawData)
    {
        $params = func_get_args();
        $params[0] = $this->prepareRawData($rawData);
        $this->rawData = call_user_func_array([$this, 'checkRawData'], $params);
    }

    /**
     * Проверка полученных данных.
     *
     * @param array $rawData Полученные данные.
     *
     * @throws \TextMedia\PlagiarismApi\Exception\ResponseException
     *
     * @return array
     */
    abstract protected function checkRawData(array $rawData): array;

    /**
     * Подготовка полученных данных к проверке.
     *
     * @param mixed $rawData Полученные данные (JSON-строка или массив).
     *
     * @throws \TextMedia\PlagiarismApi\Exception\ResponseException
     *
     * @return array
     */
    protected function prepareRawData($rawData): array
    {
        if (is_string($rawData)) {
            $rawData = $this->parseJson($rawData);
        } elseif (!is_array($rawData)) {
            throw new ResponseException('', 6, null, [gettype($rawData)], $rawData);
        }
        return $rawData;
    }

    /**
     * Разбор JSON.
     *
     * @param string  $data  JSON.
     * @param boolean $empty OPTIONAL Может ли быть результат пустым (по умолчанию FALSE).
     *
     * @throws \TextMedia\PlagiarismApi\Exception\ResponseException
     *
     * @return array
     */
    final public static function parseJson(string $data, bool $empty = false): array
    {
        $array = @json_decode($data, true);
        $error = json_last_error();
        if (JSON_ERROR_NONE !== $error) {
            throw new ResponseException('', 2, null, [$error, json_last_error_msg(), $data], $data);
        } elseif (!is_array($array)) {
            throw new ResponseException('', 3, null, [], $data);
        } elseif ($empty === false && empty($array)) {
            throw new ResponseException('', 4, null);
        } else {
            return $array;
        }
    }

    /**
     * Сохранение скрытых объектов.
     *
     * @param array $objects Набор объектов для сохранения.
     */
    final protected function setHiddenObjects(array $objects)
    {
        self::$objects[spl_object_hash($this)] = $objects;
    }

    /**
     * Получение скрытого объекта.
     *
     * @param string $name Имя объкета.
     *
     * @return mixed|NULL
     */
    final protected function getHiddenObject(string $name)
    {
        return (self::$objects[spl_object_hash($this)][$name] ?? null);
    }

    /**
     * Проверка наличия и правильности диапазона в даных.
     *
     * @param array     $data      Массив данных.
     * @param string    $start     Имя поля начала диапазона.
     * @param string    $end       Имя поля конца диапазона.
     * @param string    $prefix    OPTIONAL Префикс для имен полей (по умолчанию пустая строка).
     *
     * @throws \TextMedia\PlagiarismApi\Exception\ResponseException
     */
    final protected function checkIntegerRange(array $data, string $start, string $end, string $prefix = '')
    {
        $this->checkIntegerValue($data, $start, $prefix);
        $this->checkIntegerValue($data, $end, $prefix);

        if ($data[$start] > $data[$end]) {
            $params = ["{$prefix}{$end}", $data[$end], "{$prefix}{$start}", $data[$start]];
            $debug  = [$data, $start, $end];
            throw new ResponseException('', 5, null, $params, $debug);
        }
    }

    /**
     * Проверка наличия и правильности типа целого значения в данных.
     *
     * @param array   $data   Массив данных.
     * @param string  $name   Имя поля.
     * @param string  $prefix OPTIONAL Префикс для имени поля (по умолчанию пустая строка).
     *
     * @throws \TextMedia\PlagiarismApi\Exception\ResponseException
     *
     * @return integer
     */
    final protected function checkIntegerValue(array $data, string $name, string $prefix = ''): int
    {
        if (
            !isset($data[$name])
            || !is_scalar($data[$name])
            || !preg_match('#^\d+$#', (string) $data[$name])
            || $data[$name] < 0
        ) {
            throw new ResponseException('', 1, null, ["{$prefix}{$name}"], [$data, $name]);
        }
        return (int) $data[$name];
    }

    /**
     * Проверка наличия и правильности типа строкового значения в данных.
     *
     * @param array   $data   Массив данных.
     * @param string  $name   Имя поля.
     * @param string  $prefix OPTIONAL Префикс для имени поля (по умолчанию пустая строка).
     *
     * @throws \TextMedia\PlagiarismApi\Exception\ResponseException
     *
     * @return string
     */
    final protected function checkStringValue(array $data, string $name, string $prefix = ''): string
    {
        if (!isset($data[$name]) || !is_scalar($data[$name]) || ($data[$name] === '')) {
            throw new ResponseException('', 1, null, ["{$prefix}{$name}"], [$data, $name]);
        }
        return (string) $data[$name];
    }

    /**
     * Проверка наличия и правильности типа строкового JSON-значения в данных.
     *
     * @param array   $data   Массив данных.
     * @param string  $name   Имя поля.
     * @param string  $prefix OPTIONAL Префикс для имени поля (по умолчанию пустая строка).
     *
     * @throws \TextMedia\PlagiarismApi\Exception\ResponseException
     *
     * @return string
     */
    final protected function checkJsonStringValue(array $data, string $name, string $prefix = ''): string
    {
        $data = $this->checkStringValue($data, $name, $prefix);
        $data = $this->parseJson($data, true);
        return (string) json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    }

    /**
     * Проверка наличия и правильности массива в данных.
     *
     * @param array   $data   Массив данных.
     * @param string  $name   Имя поля.
     * @param string  $prefix OPTIONAL Префикс для имени поля (по умолчанию пустая строка).
     * @param boolean $empty  OPTIONAL Может ли массив быть пустым (по умолчанию FALSE).
     *
     * @throws \TextMedia\PlagiarismApi\Exception\ResponseException
     *
     * @return array
     */
    final protected function checkArrayValue(array $data, string $name, string $prefix = '', bool $empty = false): array
    {
        if (!isset($data[$name]) || !is_array($data[$name])) {
            throw new ResponseException('', 1, null, ["{$prefix}{$name}"], [$data, $name]);
        } elseif (!$empty && empty($data[$name])) {
            throw new ResponseException('', 1, null, ["{$prefix}{$name}"], [$data, $name]);
        }
        return $data[$name];
    }

    /**
     * Проверка наличия и правильности массива чисел в виде строки в данных (например, "12 34 56").
     *
     * @param array   $data   Массив данных.
     * @param string  $name   Имя поля.
     * @param string  $prefix OPTIONAL Префикс для имени поля (по умолчанию пустая строка).
     *
     * @throws \TextMedia\PlagiarismApi\Exception\ResponseException
     *
     * @return array
     */
    final protected function checkStrIntArray(array $data, string $name, string $prefix = ''): array
    {
        $this->checkStringValue($data, $name, $prefix);
        return empty($data[$name]) ? [] : array_map(function ($value) use ($prefix, $data, $name): int {
            if (!preg_match('#^\d+$#', $value)) {
                throw new ResponseException('', 1, null, ["{$prefix}$name"], [$data, $name]);
            }
            return (int) $value;
        }, explode(' ', $data[$name]));
    }

    /**
     * Проверка наличия и правильности типа процентного значения в данных.
     *
     * @param array   $data   Массив данных.
     * @param string  $name   Имя поля.
     * @param string  $prefix OPTIONAL Префикс для имени поля (по умолчанию пустая строка).
     *
     * @throws \TextMedia\PlagiarismApi\Exception\ResponseException
     *
     * @return string
     */
    final protected function checkPercentValue(array $data, string $name, string $prefix = ''): string
    {
        $checked = isset($data[$name]) ? $this->checkPercent($data[$name]) : false;
        if ($checked === false) {
            throw new ResponseException('', 1, null, ["{$prefix}{$name}"], [$data, $name]);
        }
        return $checked;
    }

    /**
     * Проверка наличия и правильности типа URL-значения в данных.
     *
     * @param array   $data   Массив данных.
     * @param string  $name   Имя поля.
     * @param string  $prefix OPTIONAL Префикс для имени поля (по умолчанию пустая строка).
     * @param boolean $strict OPTIONAL Жесткая проверка (не игнорировать ошибки) (по умолчанию TRUE).
     *
     * @throws \TextMedia\PlagiarismApi\Exception\ResponseException
     *
     * @return string
     */
    final protected function checkUrlValue(array $data, string $name, string $prefix = '', bool $strict = true): string
    {
        $checked = $this->checkStringValue($data, $name, $prefix);
        if ($strict) {
            $checked = $this->checkUrl($data[$name], true, true);
            if ($checked === false) {
                echo $data[$name];
                throw new ResponseException('', 1, null, ["{$prefix}{$name}"], [$data, $name]);
            }
        }
        return $checked;
    }

    /**
     * Проверка наличия и правильности типа UID-значения в данных.
     *
     * @param array   $data   Массив данных.
     * @param string  $name   Имя поля.
     * @param string  $prefix OPTIONAL Префикс для имени поля (по умолчанию пустая строка).
     *
     * @throws \TextMedia\PlagiarismApi\Exception\ResponseException
     *
     * @return string
     */
    final protected function checkUidValue(array $data, string $name, string $prefix = ''): string
    {
        $this->checkStringValue($data, $name, $prefix);
        $checked = $this->checkUid($data[$name]);
        if ($checked === false) {
            throw new ResponseException('', 1, null, ["{$prefix}{$name}"], [$data, $name]);
        }
        return $checked;
    }
}
