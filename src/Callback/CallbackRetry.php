<?php

namespace TextMedia\PlagiarismApi\Callback;

/**
 * Ответ "запрос обработан с ошибкой - требуется повторить попытку через указанное число секунд".
 */
final class CallbackRetry extends CallbackResponse
{
    /** @var integer Задержка в секундах перед повтором запроса. */
    protected $delay;

    /**
     * Конструктор.
     *
     * @param integer $delay Задержка в секундах перед повтором запроса.
     */
    public function __construct(int $delay)
    {
        $this->delay = $delay;
    }

    /**
     * {@inheritdoc}
     */
    public function __toString(): string
    {
        return "retry after {$this->delay}";
    }
}
