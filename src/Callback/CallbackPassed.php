<?php

namespace TextMedia\PlagiarismApi\Callback;

/**
 * Ответ "запрос успешно обработан".
 */
final class CallbackPassed extends CallbackResponse
{
    /**
     * {@inheritdoc}
     */
    public function __toString(): string
    {
        return 'ok';
    }
}
