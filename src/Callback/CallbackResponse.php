<?php

namespace TextMedia\PlagiarismApi\Callback;

/**
 * Абстракция для ответов, выдаваемых callback-контроллерами.
 */
abstract class CallbackResponse
{
    /**
     * Приведение ответа к строке.
     *
     * @return string
     */
    abstract public function __toString(): string;
}
