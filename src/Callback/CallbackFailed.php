<?php

namespace TextMedia\PlagiarismApi\Callback;

/**
 * Ответ "запрос обработан с ошибкой - требуется прекратить попытки".
 */
final class CallbackFailed extends CallbackResponse
{
    /**
     * {@inheritdoc}
     */
    public function __toString(): string
    {
        return 'stop trying';
    }
}
