<?php

namespace TextMedia\PlagiarismApi\Callback;

use Exception;
use TextMedia\PlagiarismApi\Response\PlagiarismResultResponse;

/**
 * Шаблон для callback-контроллеров.
 */
trait CallbackTrait
{
    /** @var array Данные, полученные от API. */
    protected $rawData;

    /** @var \TextMedia\PlagiarismApi\Response\PlagiarismResultResponse */
    protected $plagiarism;

    /** @var \Exception Выброшенное исключение. */
    protected $exception;

    /**
     * Обработка полученных данных.
     *
     * @param array $rawData OPTIONAL Данные, полученные от API (по умолчанию NULL).
     */
    final public function process($rawData = null)
    {
        if (!empty($rawData)) {
            $this->rawData = $rawData;
        } elseif (empty($this->rawData)) {
            $this->rawData = filter_input_array(INPUT_POST);
        }

        try { // Пробуем сформировать объект-ответ и обработать его.
            $this->plagiarism = new PlagiarismResultResponse($this->rawData, true);
            $result = $this->onReceipt();
        } catch (Exception $ex) { // Произошла ошибка - обрабатываем ее.
            $this->exception = $ex;
            $result = $this->onError();
        }

        print($result) and exit;
    }
}
