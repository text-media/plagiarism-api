<?php

namespace TextMedia\PlagiarismApi\Callback;

/**
 * Шаблон для callback-контроллеров.
 */
abstract class CallbackTemplate
{
    use CallbackTrait;

    /**
     * Обработка полученных данных.
     *
     * @return \TextMedia\PlagiarismApi\Callback\CallbackResponse
     */
    abstract protected function onReceipt(): CallbackResponse;

    /**
     * Обработка ошибки.
     *
     * @return \TextMedia\PlagiarismApi\Callback\CallbackResponse
     */
    abstract protected function onError(): CallbackResponse;
}
