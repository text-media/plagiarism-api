<?php

namespace TextMedia\PlagiarismApi;

use TextMedia\PlagiarismApi\Exception\ClientException;
use TextMedia\PlagiarismApi\Request\CheckTextRequest;
use TextMedia\PlagiarismApi\Request\PackageBalanceRequest;
use TextMedia\PlagiarismApi\Request\PlagiarismResultRequest;
use TextMedia\PlagiarismApi\Request\RequestInterface;
use TextMedia\PlagiarismApi\Response\CheckTextResponse;
use TextMedia\PlagiarismApi\Response\PackageBalanceResponse;
use TextMedia\PlagiarismApi\Response\PlagiarismResultResponse;
use TextMedia\PlagiarismApi\Response\ResponseInterface;
use TextMedia\PlagiarismApi\Transport\AbstractTransport;
use TextMedia\PlagiarismApi\Transport\TransportInterface;

/**
 * Клиент API проверки уникальности текста от TextMedia.
 */
final class Client
{
    /** Имя поля с API-ключом пользователя. */
    const USERKEY_FIELD = 'userkey';

    /** Имя переменной окружения, где может быть задан API-ключ по умолчанию. */
    const USERKEY_ENV = 'TMPA_USER_KEY';

    /** Точка входа по умолчанию. */
    const ENTRY_POINT = 'http://api.text.ru/';

    /** Имя переменной окружения, где может быть переопределена точка входа. */
    const ENTRY_POINT_ENV = 'TMPA_ENTRY_POINT';

    /** @var string Ключ пользователя. */
    protected $userKey;

    /** @var \TextMedia\PlagiarismApi\Transport\TransportInterface Транспорт. */
    protected $transport;

    protected static $env = [];

    /**
     * Констурктор.
     *
     * @param string                                                $userKey   OPTIONAL API-ключ пользователя.
     *                                                                         (по умолчанию определяется из окружения).
     * @param \TextMedia\PlagiarismApi\Transport\TransportInterface $transport OPTIONAL Транспорт
     *                                                                         (по умолчанию определяется из окружения).
     */
    public function __construct(string $userKey = null, TransportInterface $transport = null)
    {
        $this
            ->setUserKey($userKey)
            ->setTransport($transport)
        ;
    }

    /**
     * Преобразование в строку.
     *
     * @return string
     */
    public function __toString(): string
    {
        return (string) json_encode(
            [
                'userKey'   => $this->userKey,
                'transport' => (string) $this->transport,
            ],
            JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE
        );
    }

    /**
     * Восстановление из массива (после var_export).
     *
     * @param array $data Данные для восстановления.
     *
     * @return self
     */
    public static function __set_state(array $data): self
    {
        return new self(
            $data['userKey'] ?? null,
            $data['transport'] ?? null
        );
    }

    /**
     * @return string Ключ пользователя.
     */
    public function getUserKey(): string
    {
        return $this->userKey;
    }

    /**
     * @param string $userKey Ключ пользователя.
     *
     * @throws \TextMedia\PlagiarismApi\Exception\ClientException Если ключ имеет неправильный формат.
     *
     * @return $this
     */
    public function setUserKey(string $userKey = null): self
    {
        if (empty($userKey)) {
            $envKey = self::getDefaultUserKey();
            if (empty($envKey)) {
                throw new ClientException('', 3);
            }

            $userKey = $envKey;
        }

        if (!preg_match('#^[0-9a-f]{32}$#', (string) $userKey)) {
            throw new ClientException('', 1, null, [$userKey]);
        }

        $this->userKey = $userKey;
        return $this;
    }

    /**
     * Установка API-ключа по умолчанию.
     *
     * @param string $userKey API-ключ.
     */
    public static function setDefaultUserKey(string $userKey)
    {
        putenv(self::USERKEY_ENV . '=' . $userKey);
    }

    /**
     * Получение API-ключа по умолчанию.
     *
     * @return string
     */
    public static function getDefaultUserKey(): string
    {
        return (string) getenv(self::USERKEY_ENV);
    }

    /**
     * @return \TextMedia\PlagiarismApi\Transport\TransportInterface Транспорт.
     */
    public function getTransport(): TransportInterface
    {
        return $this->transport;
    }

    /**
     * @param \TextMedia\PlagiarismApi\Transport\TransportInterface Транспорт.
     *
     * @return $this
     */
    public function setTransport(TransportInterface $transport = null): self
    {
        $this->transport = $transport ?: self::getDefaultTransport();
        return $this;
    }


    /**
     * @param integer $timeout OPTIONAL Таймаут.
     * @param boolean $secure  OPTIONAL Нужно ли проверять возможность работы по HTTPS (по умолчанию FALSE).
     *
     * @return \TextMedia\PlagiarismApi\Transport\TransportInterface Транспорт по умолчанию.
     */
    public static function getDefaultTransport(
        int $timeout = TransportInterface::DEFAULT_TIMEOUT,
        bool $secure = false
    ): TransportInterface {
        return AbstractTransport::getDefault($timeout, $secure);
    }

    /**
     * Установка точки входа по умолчанию.
     *
     * @param string $userKey API-ключ.
     */
    public static function setDefaultEntryPoint(string $userKey)
    {
        putenv(self::ENTRY_POINT_ENV . '=' . $userKey);
    }

    /**
     * Получение точки входа по умолчанию.
     *
     * @return string
     */
    public static function getDefaultEntryPoint(): string
    {
        return (string) getenv(self::ENTRY_POINT_ENV);
    }

    /**
     * Выполнение запроса к API.
     *
     * @param \TextMedia\PlagiarismApi\Request\RequestInterface $request Запрос.
     *
     * @return \TextMedia\PlagiarismApi\Response\ResponseInterface
     */
    public function sendRequest(RequestInterface $request): ResponseInterface
    {
        $response = str_replace('Request', 'Response', get_class($request));
        return new $response($this->transport->process(
            (self::getDefaultEntryPoint() ?: self::ENTRY_POINT) . $request->getEntryPath(),
            array_replace($request->getRawData(), [self::USERKEY_FIELD => $this->userKey])
        ));
    }

    /**
     * Получение результата запроса к API на запрос "отправить текст на проверку".
     *
     * @param \TextMedia\PlagiarismApi\Request\CheckTextRequest|array $data Запрос.
     *
     * @return \TextMedia\PlagiarismApi\Response\CheckTextResponse
     */
    public function sendTextToCheck($data): CheckTextResponse
    {
        return $this->sendRequest($this->checkRequestType($data, CheckTextRequest::class));
    }

    /**
     * Получение результата запроса к API на запрос "получить результат проверки текста".
     *
     * @param \TextMedia\PlagiarismApi\Request\PlagiarismResultRequest|array $data Запрос.
     *
     * @return \TextMedia\PlagiarismApi\Response\PlagiarismResultResponse
     */
    public function getPlagiarismResult($data): PlagiarismResultResponse
    {
        return $this->sendRequest($this->checkRequestType($data, PlagiarismResultRequest::class));
    }

    /**
     * Получение результата запроса к API на запрос "получить информацию по пакетам".
     *
     * @return \TextMedia\PlagiarismApi\Response\PackageBalanceResponse
     */
    public function getPackageBalance(): PackageBalanceResponse
    {
        return $this->sendRequest(new PackageBalanceRequest());
    }

    /**
     * Проверка запроса и возврата его в объекте.
     *
     * @param \TextMedia\PlagiarismApi\Request\RequestInterface|array $data     Запрос или данные для него.
     * @param string                                                  $expected Какого класса должен быть запрос.
     *
     * @throws \TextMedia\PlagiarismApi\Exception\ClientException
     *
     * @return \TextMedia\PlagiarismApi\Request\RequestInterface
     */
    protected function checkRequestType($data, string $expected): RequestInterface
    {
        if (is_array($data)) {
            return new $expected($data);
        } elseif (is_object($data) && get_class($data) === $expected) {
            return $data;
        } else {
            throw new ClientException('', 2, null, [$expected, gettype($data)]);
        }
    }
}
