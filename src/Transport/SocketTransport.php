<?php

namespace TextMedia\PlagiarismApi\Transport;

use TextMedia\PlagiarismApi\Exception\TransportException;

/**
 * Транспорт запросов к API при помощи сокетов.
 */
final class SocketTransport extends AbstractTransport
{
    /**
     * {@inheritDoc}
     *
     * @see https://www.php.net/manual/ru/book.sockets.php
     */
    public static function check(bool $secure = false)
    {
        if (!extension_loaded('sockets')) {
            throw new TransportException('', 13);
        } elseif ($secure) {
            throw new TransportException('', 7);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function processHttp(string $entryPoint, array $request = null): array
    {
        $data = parse_url($entryPoint);
        if (filter_var($data['host'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
            $data['addr'] = $data['host'];
            $data['type'] = AF_INET;
        } elseif (filter_var($data['host'], FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
            $data['addr'] = $data['host'];
            $data['type'] = AF_INET6;
        } else {
            $addr = gethostbyname($data['host']);
            if (filter_var($addr, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
                $data['addr'] = $addr;
                $data['type'] = AF_INET;
            } else {
                throw new TransportException('', 8, null, [$data['host']]);
            }
        }

        $options = [
            "POST {$data['path']} HTTP/1.1",
            "Host: {$data['host']}",
            "Connection: close",
            "Cache-Control: no-cache",
        ];
        if ($request) {
            $request = http_build_query($request);
            $options[] = "Content-Type: application/x-www-form-urlencoded";
            $options[] = "Content-Length: " . strlen($request);
            $options[] = "";
            $options[] = $request;
        }

        $debugInfo = [
            'error'   => ['message' => null, 'number' => null],
            'options' => $options,
            'headers' => [],
            'body'    => null,
            'time'    => null,
        ];

        $time   = microtime(true);
        $socket = socket_create($data['type'], SOCK_STREAM, SOL_TCP);
        if (
            $socket === false
            || !socket_set_option($socket, SOL_SOCKET, SO_SNDTIMEO, ['sec' => $this->getTimeout(), 'usec' => 0])
            || !socket_connect($socket, $data['addr'], (int) ($data['port'] ?? 80))
        ) {
            $debugInfo['error']['number'] = socket_last_error();
        }

        $response = '';
        if (is_resource($socket) && empty($debugInfo['error']['number'])) {
            $request = implode("\r\n", $options) . "\r\n\r\n";
            $length  = strlen($request);
            $wrote   = socket_write($socket, $request, $length);
            if (empty($wrote) || $wrote !== $length) {
                $debugInfo['error']['number'] = socket_last_error();
            } else {
                while ($line = socket_read($socket, 0x400)) {
                    $response .= $line;
                }
            }
        }

        if ($socket !== false) {
            socket_close($socket);
        }
        $debugInfo['time'] = microtime(true) - $time;

        $parts   = explode("\r\n\r\n", $response, 2);
        $body    = trim((string) array_pop($parts));
        $headers = trim((string) array_pop($parts));

        $debugInfo['headers']['http_code'] = null;
        foreach ((explode("\n", $headers) ?: []) as $line) {
            $line = trim($line);
            if (preg_match('#^HTTP/\d\.\d \d+ .*#', $line)) {
                $debugInfo['headers']['http_code'] = (int) explode(' ', $line, 3)[1];
            } elseif (preg_match('#^\S+:.+$#', $line)) {
                list($name, $value) = explode(':', $line);
                $debugInfo['headers'][strtolower($name)] = trim($value);
            }
        }

        if (preg_match('#^([A-Fa-f\d]+)\r\n#', $body, $match) && substr($body, -1) === '0') {
            $length = $match[1];
            $body   = substr($body, 2 + strlen($length), (int) hexdec($length));
        }
        $debugInfo['body'] = $body;

        if (null !== $debugInfo['error']['number']) {
            $debugInfo['error']['message'] = socket_strerror($debugInfo['error']['number']);
            throw new TransportException('', 9, null, array_values($debugInfo['error']), $debugInfo);
        }

        return [(int) ($debugInfo['headers']['http_code'] ?? null), $debugInfo['body'], $debugInfo];
    }
}
