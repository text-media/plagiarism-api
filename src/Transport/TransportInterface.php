<?php

namespace TextMedia\PlagiarismApi\Transport;

/**
 * Интерфейс транспорта запросов к API.
 */
interface TransportInterface
{
    /** Таймаут по умолчанию. */
    const DEFAULT_TIMEOUT = 10;

    /** Минимальный таймаут. */
    const MINIMAL_TIMEOUT = 5;

    /** Максимальный таймаут. */
    const MAXIMAL_TIMEOUT = 60;

    /**
     * Конструктор.
     *
     * @param integer $timeout OPTIONAL Таймаут.
     *
     * @throws \TextMedia\PlagiarismApi\Exception\TransportException
     */
    public function __construct(int $timeout = self::DEFAULT_TIMEOUT);

    /**
     * Преобразование в строку.
     *
     * @return string
     */
    public function __toString(): string;

    /**
     * Восстановление из массива (после var_export).
     *
     * @param array $data Данные для восстановления.
     *
     * @throws \TextMedia\PlagiarismApi\Exception\TransportException
     *
     * @return self
     */
    public static function __set_state(array $data): TransportInterface;

    /**
     * @return integer Значение таймаута.
     */
    public function getTimeout(): int;

    /**
     * Установка таймаута.
     *
     * @param integer $timeout Таймаут.
     *
     * @throws \TextMedia\PlagiarismApi\Exception\TransportException
     *
     * @return $this
     */
    public function setTimeout(int $timeout): TransportInterface;

    /**
     * Отправка запроса - возврат ответа.
     *
     * @param string $entryPoint Точка входа.
     * @param array  $request    OPTIONAL Запрос (по умолчанию NULL).
     *
     * @throws \TextMedia\PlagiarismApi\Exception\TransportException
     *
     * @return string
     */
    public function process(string $entryPoint, array $request = null): string;

    /**
     * Проверка доступности данного вида транспорта.
     *
     * @param boolean $secure OPTIONAL Нужно ли проверять возможность работы по HTTPS (по умолчанию FALSE).
     *
     * @throws \TextMedia\PlagiarismApi\Exception\TransportException
     */
    public static function check(bool $secure = false);
}
