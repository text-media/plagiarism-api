<?php

namespace TextMedia\PlagiarismApi\Transport;

use TextMedia\PlagiarismApi\Exception\TransportException;

/**
 * Транспорт запросов к API при помощи модуля php-curl.
 */
final class CurlTransport extends AbstractTransport
{
    /**
     * {@inheritDoc}
     *
     * @see https://www.php.net/manual/ru/book.curl.php
     */
    public static function check(bool $secure = false)
    {
        if (!extension_loaded('curl')) {
            throw new TransportException('', 12);
        } elseif ($secure) {
            // nothing to do
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function processHttp(string $entryPoint, array $request = null): array
    {
        // Настраиваем CURL.
        $options = [
            CURLOPT_URL            => $entryPoint,
            CURLOPT_TIMEOUT        => $this->timeout,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER         => false,
            CURLOPT_NOBODY         => false,
            CURLOPT_HTTPGET        => is_null($request),
            CURLOPT_POST           => !is_null($request),
            CURLOPT_HTTPHEADER     => ['Expect:'],
            CURLOPT_FAILONERROR    => false,
        ];
        if ($options[CURLOPT_POST]) {
            $options[CURLOPT_POSTFIELDS] = $request;
        }

        // Выполняем запрос.
        $time = microtime(true);
        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $body    = trim((string) curl_exec($ch));
        $headers = curl_getinfo($ch);
        $error   = [
            'message' => curl_error($ch),
            'number'  => curl_errno($ch),
        ];
        curl_close($ch);

        $debugInfo = [
            'options' => $options,
            'headers' => $headers,
            'error'   => $error,
            'body'    => $body,
            'time'    => microtime(true) - $time,
        ];

        if (CURLE_OK !== $error['number']) {
            throw new TransportException('', 4, null, array_values($error), $debugInfo);
        }

        // Отдаем код и тело ответа.
        return [(int) ($headers['http_code'] ?? null), $body, $debugInfo];
    }
}
