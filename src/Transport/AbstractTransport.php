<?php

namespace TextMedia\PlagiarismApi\Transport;

use TextMedia\PlagiarismApi\Exception\TransportException;

/**
 * Абстракция транспорта запросов к API.
 */
abstract class AbstractTransport implements TransportInterface
{
    /** @var integer Таймаут. */
    protected $timeout;

    /**
     * {@inheritdoc}
     */
    final public function __construct(int $timeout = self::DEFAULT_TIMEOUT)
    {
        static::check();
        $this->setTimeout($timeout);
    }

    /**
     * {@inheritdoc}
     */
    final public function __toString(): string
    {
        return (string) json_encode(
            ['timeout' => $this->getTimeout()],
            JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE
        );
    }

    /**
     * {@inheritdoc}
     */
    final public static function __set_state(array $data): TransportInterface
    {
        return new static($data['timeout'] ?? self::DEFAULT_TIMEOUT);
    }

    /**
     * Получение транспорта по умолчанию.
     *
     * @param integer $timeout OPTIONAL Таймаут.
     * @param boolean $secure  OPTIONAL Нужно ли проверять возможность работы по HTTPS (по умолчанию FALSE).
     *
     * @throws \TextMedia\PlagiarismApi\Exception\TransportException
     *
     * @return \TextMedia\PlagiarismApi\Transport\TransportInterface
     */
    final public static function getDefault(
        int $timeout = self::DEFAULT_TIMEOUT,
        bool $secure = false
    ): TransportInterface {
        static $transports = [
            StreamTransport::class,
            CurlTransport::class,
            SocketTransport::class,
        ];

        $errors = [];
        foreach ($transports as $transport) {
            try {
                $transport::check($secure);
                return new $transport($timeout);
            } catch (TransportException $ex) {
                $errors[] = "{$transport}: {$ex->getMessage()}";
            }
        }

        throw new TransportException('', 10, null, [implode("\n", $errors)]);
    }

    /**
     * {@inheritdoc}
     */
    final public function getTimeout(): int
    {
        return $this->timeout;
    }

    /**
     * {@inheritdoc}
     */
    final public function setTimeout(int $timeout): TransportInterface
    {
        if ($timeout < self::MINIMAL_TIMEOUT || self::MAXIMAL_TIMEOUT < $timeout) {
            throw new TransportException('', 1, null, [$timeout, self::MINIMAL_TIMEOUT, self::MAXIMAL_TIMEOUT]);
        }

        $this->timeout = $timeout;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    final public function process(string $entryPoint, array $request = null): string
    {
        $data = parse_url($entryPoint);
        if (
            !is_array($data)
            || !isset($data['scheme'])
            || !isset($data['host'])
            || !isset($data['path'])
            || !is_string($data['scheme'])
            || !preg_match('#^https?$#i', $data['scheme'])
        ) {
            throw new TransportException('', 6, null, [$entryPoint]);
        }

        static::check(strtolower($data['scheme']) === 'https');
        list($code, $body, $debug) = $this->processHttp($entryPoint, $request);

        if (empty($code) || 200 !== $code) {
            throw new TransportException('', 2, null, [$code], $debug);
        } elseif (empty($body)) {
            throw new TransportException('', 3, null, [], $debug);
        }

        return $body;
    }

    /**
     * Отправка запроса - возврат ответа.
     *
     * @param string $entryPoint Точка входа.
     * @param array  $request    OPTIONAL Запрос (по умолчанию NULL).
     *
     * @throws \TextMedia\PlagiarismApi\Exception\TransportException
     *
     * @return array [int Код HTTP, string тело ответа, массив с отладочной информацией].
     */
    abstract protected function processHttp(string $entryPoint, array $request = null): array;
}
