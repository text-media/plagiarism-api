<?php

namespace TextMedia\PlagiarismApi\Transport;

use TextMedia\PlagiarismApi\Exception\TransportException;

/**
 * Транспорт запросов к API при помощи стандартных функций PHP.
 */
final class StreamTransport extends AbstractTransport
{
    /**
     * {@inheritDoc}
     *
     * @see https://www.php.net/manual/ru/filesystem.configuration.php#ini.allow-url-fopen
     */
    public static function check(bool $secure = false)
    {
        if (empty(ini_get('allow_url_fopen'))) {
            throw new TransportException('', 11);
        } elseif ($secure) {
            // nothing to do
        }
    }

    /**
     * {@inheritdoc}
     *
     * @see https://www.php.net/manual/ru/context.http.php
     */
    protected function processHttp(string $entryPoint, array $request = null): array
    {
        $options = ['http' => [
            'method'           => 'POST',
            'header'           => 'Content-Type: application/x-www-form-urlencoded',
            'timeout'          => $this->getTimeout(),
            'protocol_version' => '1.1',
            'ignore_errors'    => true,
        ]];
        if ($request) {
            $options['http']['content'] = http_build_query($request);
        }
        $context = stream_context_create($options);

        $debugInfo = [
            'error'   => ['message' => null, 'number' => null],
            'options' => $options,
            'headers' => [],
            'body'    => null,
            'time'    => null,
        ];

        set_error_handler(function ($code, $msg, $file, $line) use (&$debugInfo): bool {
            $debugInfo['error'] = [
                'message' => "{$msg} in {$file}[{$line}]",
                'number'  => $code,
            ];
            return true;
        });
        $time = microtime(true);

        $stream = fopen($entryPoint, 'r', false, $context);
        if ($stream !== false) {
            $debugInfo['body']    = trim((string) stream_get_contents($stream));
            $debugInfo['headers'] = stream_get_meta_data($stream);
            fclose($stream);
        }

        $debugInfo['time'] = microtime(true) - $time;
        restore_error_handler();

        if (
            isset($debugInfo['headers']['wrapper_data'])
            && isset($debugInfo['headers']['wrapper_data'][0])
            && preg_match('#^HTTP/\d\.\d \d+ .*#', $debugInfo['headers']['wrapper_data'][0])
        ) {
            $debugInfo['headers']['http_code'] = (int) explode(' ', $debugInfo['headers']['wrapper_data'][0], 3)[1];
        }

        if (null !== $debugInfo['error']['number']) {
            throw new TransportException('', 5, null, array_values($debugInfo['error']), $debugInfo);
        }

        return [(int) ($debugInfo['headers']['http_code'] ?? null), $debugInfo['body'], $debugInfo];
    }
}
