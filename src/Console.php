<?php

namespace TextMedia\PlagiarismApi;

use Exception;
use InvalidArgumentException;
use RuntimeException;
use TextMedia\PlagiarismApi\Response\AbstractRawData;
use TextMedia\PlagiarismApi\Transport\CurlTransport;

/**
 * Выполнение команд из консоли.
 */
final class Console
{
    /** @var array Команды. */
    protected static $commands = [
        'package_balance'    => [
            'descr'  => 'получение баланса пакетов символов',
            'method' => 'getPackageBalance',
        ],
        'check_text'  => [
            'descr'  => 'отправка текста на проверку уникальности',
            'method' => 'sendTextToCheck',
        ],
        'plagiarism_result' => [
            'descr'  => 'получение результата проверки текста',
            'method' => 'getPlagiarismResult',
            'fields' => [
                'result_json' => 'getPlagiarismDetail',
                'spell_check' => 'getSpellingDetail',
                'seo_check'   => 'getSeoAnalyzeDetail',
            ],
        ],
    ];

    /** @var array Команды для PHAR. */
    protected static $pharCommands = [
        'self_update' => [
            'descr'  => 'обновление скрипта до последней версии',
            'method' => 'selfUpdate',
        ],
        'version'     => [
            'descr'  => 'вывод версии скрипта',
            'method' => 'version',
        ],
    ];

    /**
     * Выполнение команды.
     *
     * @global integer  $argc
     * @global string[] $argv
     *
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public static function execute()
    {
        global $argc, $argv;

        try {
            if (PHP_SAPI !== 'cli') {
                throw new RuntimeException('Запуск скрипта возможен только из консоли.');
            }

            if (substr(__FILE__, 0, 7) === 'phar://') {
                self::$commands = array_replace(self::$commands, self::$pharCommands) ?: [];
            }

            if ($argc < 2) {
                print(implode('', array_merge(
                    [
                        "Формат запуска: php {$argv[0]} <команда> [опции]\n",
                        "Доступные команды:\n",
                    ],
                    array_map(function (string $name, array $info) {
                        return str_pad("    {$name}:", 24) . "{$info['descr']}\n";
                    }, array_keys(self::$commands), self::$commands),
                    [
                        "Опции команд передаются в виде: --<имя опции>=<значение>\n",
                        "Имена и значения опций аналогичны тем, что описаны в документации API\n",
                        "   --text=<путь>:   не содержит сам текст, а только путь к файлу с ним\n",
                        "   --return=<поле>: указывает, какое поле ответа необходимо вернуть\n",
                    ]
                ))) and exit(0);
            }

            $command = $argv[1];
            if (!isset(self::$commands[$command])) {
                throw new InvalidArgumentException("Указана неизвестная команда: '{$command}'");
            } elseif (is_callable([self::class, self::$commands[$command]['method']])) {
                $result = call_user_func([self::class, self::$commands[$command]['method']]);
                print("{$result}\n") and exit(0);
            }

            $userKey = null;
            $options = [];
            $return  = null;
            for ($num = 2, $len = count($argv); $num < $len; $num++) {
                if (!preg_match('#^--(\w+)=(.+)$#', $argv[$num], $match)) {
                    throw new InvalidArgumentException("Неправильный формат передачи опции: '{$argv[$num]}'");
                } else {
                    $value = trim(array_pop($match), " \t\n\r\0\x0B'\"");
                    $name  = array_pop($match);
                    if ($name === 'return') {
                        $return = $value;
                    } elseif ($name === Client::USERKEY_FIELD) {
                        $userKey = $value;
                    } else {
                        $options[$name] = $value;
                    }
                }
            }
            if (is_null($userKey)) {
                throw new RuntimeException('Не передано значение API-ключа');
            }

            if (isset($options[Request::OPT_TEXT])) {
                if (!is_file($options[Request::OPT_TEXT]) || !is_readable($options[Request::OPT_TEXT])) {
                    throw new RuntimeException(
                        "Файл '{$options[Request::OPT_TEXT]}' не найден или не доступен для чтения"
                    );
                } else {
                    $options[Request::OPT_TEXT] = file_get_contents($options[Request::OPT_TEXT]);
                }
            }

            $command = self::$commands[$command];
            $client  = new Client($userKey);
            $result  = call_user_func([$client, $command['method']], $options);
            if (!empty($return)) {
                if (!empty($command['fields']) && isset($command['fields'][$return])) {
                    $result = call_user_func([$result, $command['fields'][$return]]);
                    $result = is_array($result)
                        ? json_encode(
                            array_map(function ($item) {
                                return $item->getRawData();
                            }, $result),
                            JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
                        )
                        : (string) $result;
                } elseif (isset($result->getRawData()[$return])) {
                    $result = $result->getRawData()[$return];
                } else {
                    throw new InvalidArgumentException("Поле '{$return}' отсутствует в ответе");
                }
            }
            print($result) and exit(0);
        } catch (Exception $ex) {
            $er = get_class($ex) . "[{$ex->getCode()}]: {$ex->getMessage()}\n";
            $fe = fopen('php://stderr', 'w');
            if ($fe !== false) {
                fwrite($fe, $er);
                fclose($fe);
            } else {
                print($er);
            }
            exit(0xff);
        }
    }

    /** @return string Версия. */
    protected static function version(): string
    {
        return VERSION;
    }

    /**
     * Самообновление.
     *
     * @throws \RuntimeException
     *
     * @return string
     */
    protected static function selfUpdate(): string
    {
        if (!is_writable(SCRIPT)) {
            throw new RuntimeException(
                'Обновление не может быть выпролнено, т.к. файл \'' . SCRIPT . '\' не доступен для записи'
            );
        }
        $repoName  = 'text-media/plagiarism-api';
        $bitbucket = "https://bitbucket.org/{$repoName}/raw/";
        $transport = new CurlTransport();
        $versions  = $transport->process("https://api.bitbucket.org/2.0/repositories/{$repoName}/refs/tags");
        $versions  = AbstractRawData::parseJson($versions);
        $lastVer   = VERSION;
        $hashVer   = null;
        foreach ($versions['values'] as $version) {
            if (version_compare($version['name'], $lastVer) !== -1) {
                $lastVer = $version['name'];
                $hashVer = $version['target']['hash'];
            }
        }
        if (VERSION === $lastVer) {
            return "Установлена последняя версия {$lastVer}";
        } elseif (strtolower(readline("Обновить скрипт до версии {$lastVer} [y/n]: ")) !== 'y') {
            return "Обновление до {$lastVer} отменено";
        } else {
            $bitbucket .= "/{$hashVer}";
            $fileHash = $transport->process("{$bitbucket}/bin/tmpa.phar");
            $fileData = $transport->process("{$bitbucket}/bin/tmpa.hash");
            if (md5($fileData) !== $fileHash) {
                throw new RuntimeException('Ошибка скачивания файла с bitbucket (не совпадают хэши)');
            } elseif (strlen($fileData) !== file_put_contents(SCRIPT, $fileData)) {
                throw new RuntimeException('Ошибка записи в конечный файл ' . SCRIPT);
            } else {
                return "Скрипт успешно обновлен до версии {$lastVer}";
            }
        }
    }
}
