<?php

namespace TextMedia\PlagiarismApi\Request;

use TextMedia\PlagiarismApi\Common\CheckUidTrait;
use TextMedia\PlagiarismApi\Exception\RequestException;
use TextMedia\PlagiarismApi\Request;

/**
 * Запрос к API "получить результат проверки текста на уникальность".
 */
final class PlagiarismResultRequest extends AbstractRequest
{
    use CheckUidTrait;

    /**
     * {@inheritdoc}
     */
    public function __construct(array $rawData = null)
    {
        parent::__construct($rawData);
        $this
            ->setTextUid($this->rawData[Request::OPT_TEXT_UID] ?? null)
            ->setDetailedResult($this->rawData[Request::OPT_DETAILED_RESULT] ?? null);
    }

    /**
     * @param string $textUid Уникальный идентификатор текста.
     *
     * @throws \TextMedia\PlagiarismApi\Exception\RequestException
     *
     * @return $this
     */
    public function setTextUid(string $textUid): self
    {
        if ($this->checkUid($textUid) === false) {
            throw new RequestException('', 1, null, [Request::OPT_TEXT_UID, $textUid]);
        }

        $this->rawData[Request::OPT_TEXT_UID] = $textUid;
        return $this;
    }

    /**
     * @param boolean|string $detailedResult Требование получить более детальную информацию о результатах проверки
     *                                       (по умолчанию NULL).
     *
     * @return $this
     */
    public function setDetailedResult($detailedResult = null): self
    {
        $this->rawData[Request::OPT_DETAILED_RESULT] = !is_null($detailedResult)
            ? $this->checkBooleanValue(
                Request::OPT_DETAILED_RESULT,
                $detailedResult,
                Request::VAL_DETAILED_RESULT,
                true
            )
            : null;
        return $this;
    }

    /**
     * @return string Уникальный идентификатор текста.
     */
    public function getTextUid(): string
    {
        return $this->rawData[Request::OPT_TEXT_UID];
    }

    /**
     * @return boolean Статус требования получить более детальную информацию о результатах проверки.
     */
    public function getDetailedResult(): bool
    {
        return ($this->rawData[Request::OPT_DETAILED_RESULT] === Request::VAL_DETAILED_RESULT);
    }
}
