<?php

namespace TextMedia\PlagiarismApi\Request;

use TextMedia\PlagiarismApi\Common\RawDataTrait;
use TextMedia\PlagiarismApi\Exception\RequestException;

/**
 * Абстракция запросов к API.
 */
abstract class AbstractRequest implements RequestInterface
{
    use RawDataTrait;

    /** Пути относительно точки входа. */
    const ENTRY_PATH = [
        CheckTextRequest::class        => 'post',
        PackageBalanceRequest::class   => 'account',
        PlagiarismResultRequest::class => 'post',
    ];

    /**
     * {@inheritdoc}
     */
    public function __construct(array $rawData = null)
    {
        $this->rawData = $rawData ?: [];
    }

    /**
     * {@inheritdoc}
     */
    final public function getEntryPath(): string
    {
        return self::ENTRY_PATH[get_called_class()];
    }

    /**
     * Проверка включения/выключения опции.
     *
     * @param string         $type     Тип опции.
     * @param string|boolean $value    Значение (строковое или логическое).
     * @param string         $string   Правильное строковое значение.
     * @param boolean        $expected В каком случае вернуть строку, если передано логическое значение.
     *
     * @throws \TextMedia\PlagiarismApi\Exception\RequestException
     *
     * @return string|NULL
     */
    final protected function checkBooleanValue(string $type, $value, string $string, bool $expected)
    {
        if (!is_bool($value) && !(is_string($value) && $value === $string)) {
            throw new RequestException('', 5, null, [$type, $string]);
        }

        return is_bool($value) ? ($value === $expected ? $string : null) : $string;
    }
}
