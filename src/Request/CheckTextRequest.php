<?php

namespace TextMedia\PlagiarismApi\Request;

use TextMedia\PlagiarismApi\Common\CheckUrlTrait;
use TextMedia\PlagiarismApi\Exception\RequestException;
use TextMedia\PlagiarismApi\Request;

/**
 * Запрос к API "отправить текст на проверку уникальности".
 */
final class CheckTextRequest extends AbstractRequest
{
    use CheckUrlTrait;

    /** Минимальный размер текста. */
    const TEXT_MIN_SIZE = 100;

    /** Максимальный размер текста. */
    const TEXT_MAX_SIZE = 150000;

    /**
     * {@inheritdoc}
     */
    public function __construct(array $rawData = null)
    {
        parent::__construct($rawData);
        $this
            ->setText($this->rawData[Request::OPT_TEXT] ?? null)
            ->setExceptDomains($this->rawData[Request::OPT_EXCEPT_DOMAINS] ?? null)
            ->setExceptUrls($this->rawData[Request::OPT_EXCEPT_URLS] ?? null)
            ->setVisibility($this->rawData[Request::OPT_VISIBILITY] ?? null)
            ->setArchive($this->rawData[Request::OPT_ARCHIVE] ?? null)
            ->setCallback($this->rawData[Request::OPT_CALLBACK] ?? null);
    }

    /**
     * @param string $text Проверяемый текст.
     *
     * @throws \TextMedia\PlagiarismApi\Exception\RequestException
     *
     * @return $this
     */
    public function setText(string $text): self
    {
        $length = mb_strlen($text);
        if ($length < self::TEXT_MIN_SIZE || self::TEXT_MAX_SIZE < $length) {
            throw new RequestException('', 2, null, [$length, self::TEXT_MIN_SIZE, self::TEXT_MAX_SIZE]);
        }

        $this->rawData[Request::OPT_TEXT] = $text;
        return $this;
    }

    /**
     * @param array|string $domains OPTIONAL Домены, которые вы хотите исключить из проверки (по умолчанию NULL).
     *
     * @return $this
     */
    public function setExceptDomains($domains = null): self
    {
        $this->rawData[Request::OPT_EXCEPT_DOMAINS] = !empty($domains)
            ? $this->checkExceptList(Request::OPT_EXCEPT_DOMAINS, $domains)
            : null;
        return $this;
    }

    /**
     * @param array|string $urls OPTIONAL Ссылки, которые вы хотите исключить из проверки (по умолчанию NULL).
     *
     * @return $this
     */
    public function setExceptUrls($urls = null): self
    {
        $this->rawData[Request::OPT_EXCEPT_URLS] = !empty($urls)
            ? $this->checkExceptList(Request::OPT_EXCEPT_URLS, $urls)
            : null;
        return $this;
    }

    /**
     * @param string|boolean $visibility OPTIONAL Включение доступности результатов проверки другим пользователям
     *                                            (по умолчанию NULL).
     *
     * @return $this
     */
    public function setVisibility($visibility = null): self
    {
        $this->rawData[Request::OPT_VISIBILITY] = !is_null($visibility)
            ? $this->checkBooleanValue(Request::OPT_VISIBILITY, $visibility, Request::VAL_VISIBLE, true)
            : null;
        return $this;
    }

    /**
     * @param string|boolean $archive OPTIONAL Отключение формирование ссылки на результат проверки (по умолчанию NULL).
     *
     * @return $this
     */
    public function setArchive($archive = null): self
    {
        $this->rawData[Request::OPT_ARCHIVE] = !is_null($archive)
            ? $this->checkBooleanValue(Request::OPT_ARCHIVE, $archive, Request::VAL_NO_ARCHIVE, false)
            : null;
        return $this;
    }

    /**
     * @param string $callback OPTIONAL URL, куда будет отправлен запрос с результатами проверки (по умолчанию NULL).
     *
     * @throws \TextMedia\PlagiarismApi\Exception\RequestException
     *
     * @return $this
     */
    public function setCallback(string $callback = null): self
    {
        if (empty($callback)) {
            $callback = null;
        } else {
            $checked = $this->checkUrl($callback, true, true);
            if ($checked === false) {
                throw new RequestException('', 1, null, [Request::OPT_CALLBACK, $callback]);
            } else {
                $callback = $checked;
            }
        }

        $this->rawData[Request::OPT_CALLBACK] = $callback;
        return $this;
    }

    /**
     * @return string Проверяемый текст.
     */
    public function getText(): string
    {
        return $this->rawData[Request::OPT_TEXT];
    }

    /**
     * @return array Домены, которые вы хотите исключить из проверки.
     */
    public function getExceptDomains(): array
    {
        return $this->rawData[Request::OPT_EXCEPT_DOMAINS]
            ? explode(' ', $this->rawData[Request::OPT_EXCEPT_DOMAINS])
            : [];
    }

    /**
     * @return array Ссылки, которые вы хотите исключить из проверки.
     */
    public function getExceptUrls(): array
    {
        return $this->rawData[Request::OPT_EXCEPT_URLS]
            ? explode(' ', $this->rawData[Request::OPT_EXCEPT_URLS])
            : [];
    }

    /**
     * @return boolean Статус доступности результатов проверки другим пользователям.
     */
    public function getVisibility(): bool
    {
        return ($this->rawData[Request::OPT_VISIBILITY] === Request::VAL_VISIBLE);
    }

    /**
     * @return boolean Статус формирования ссылки с визуальным оформлением результатов проверки.
     */
    public function getArchive(): bool
    {
        return ($this->rawData[Request::OPT_ARCHIVE] !== Request::VAL_NO_ARCHIVE);
    }

    /**
     * @return string URL (ссылка), на которую будет отправлен POST-запрос с результатами проверки.
     */
    public function getCallback(): string
    {
        return (string) $this->rawData[Request::OPT_CALLBACK];
    }

    /**
     * Проверка списков исключений доменов/ссылок.
     *
     * @param string $type Тип списка.
     * @param mixed  $list Список (строка или массив).
     *
     * @throws \TextMedia\PlagiarismApi\Exception\RequestException
     *
     * @return string
     */
    protected function checkExceptList(string $type, $list): string
    {
        if (!is_string($list) && !is_array($list)) {
            throw new RequestException('', 3, null, [$type, gettype($list)]);
        }

        if (is_array($list)) {
            $list = implode(" ", $list);
        }
        $list = strtr($list, ["\r" => '', "\n" => ' ', ',' => ' ']);
        $list = trim((string) preg_replace('#\s\s+#', ' ', $list));
        $list = explode(' ', $list);

        $checkDomainOnly = ($type === Request::OPT_EXCEPT_DOMAINS);
        foreach ($list as $num => $item) {
            $check = $checkDomainOnly ? $this->checkDomain($item) : $this->checkUrl($item, true, true);
            if ($check === false) {
                throw new RequestException('', 4, null, [$type, $item]);
            }
            $list[$num] = $check;
        }

        return implode(' ', $list);
    }
}
