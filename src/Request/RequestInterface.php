<?php

namespace TextMedia\PlagiarismApi\Request;

/**
 * Интерфейс запросов к API.
 */
interface RequestInterface
{
    /**
     * Конструктор.
     *
     * @param array $rawData OPTIONAL Данные для запроса.
     */
    public function __construct(array $rawData = null);

    /**
     * Преобразование в строку.
     *
     * @return string
     */
    public function __toString(): string;

    /**
     * Восстановление из массива (после var_export).
     *
     * @param array $data Данные для восстановления.
     *
     * @return self
     */
    public static function __set_state(array $data);

    /**
     * @return string Путь к контроллеру относительно точки входа.
     */
    public function getEntryPath(): string;

    /**
     * @return array Данные для отправки в запросе.
     */
    public function getRawData(): array;
}
