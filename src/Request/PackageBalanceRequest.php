<?php

namespace TextMedia\PlagiarismApi\Request;

/**
 * Запрос к API "получить остаток (баланс) по пакетам символов".
 */
final class PackageBalanceRequest extends AbstractRequest
{
    /**
     * {@inheritdoc}
     */
    public function __construct(array $rawData = null)
    {
        parent::__construct(array_replace($rawData ?: [], [
            'method' => 'get_packages_info',
        ]));
    }
}
