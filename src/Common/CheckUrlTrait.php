<?php

namespace TextMedia\PlagiarismApi\Common;

/**
 * Примесь для проверки URL/доменов.
 */
trait CheckUrlTrait
{
    /**
     * Проверка URL
     *
     * @param string  $url           URL.
     * @param boolean $allowQuery    OPTIONAL Может ли содержать GET-запрос (по умолчанию FALSE).
     * @param boolean $allowFragment OPTIONAL Может ли содержать hash (по умолчанию FALSE).
     *
     * @return string|FALSE
     */
    final protected function checkUrl(string $url, bool $allowQuery = false, bool $allowFragment = false)
    {
        $parsed = parse_url($url);
        if ($parsed === false) {
            return false;
        }

        $scheme = strtolower($parsed['scheme'] ?? '');
        if (empty($scheme) || ($scheme !== 'http' && $scheme !== 'https')) {
            return false;
        }

        $user = $parsed['user'] ?? null;
        $pass = $parsed['pass'] ?? null;
        $auth = '';
        if (
            (!empty($pass) && empty($user))
            || (!empty($user) && !preg_match('#^\w+$#', '', $user))
            || (!empty($pass) && !preg_match('#^\w+$#', '', $pass))
        ) {
            return false;
        } elseif (!empty($pass) && !empty($user)) {
            $auth = $user . ($pass ? ":{$pass}" : '') . '@';
        }

        $host = $this->checkDomain($parsed['host'] ?? '');
        if (empty($host)) {
            return false;
        }

        $path = $parsed['path'] ?? '/';

        $query = $parsed['query'] ?? null;
        if (!empty($query)) {
            if (!$allowQuery) {
                return false;
            } else {
                $query = "?{$query}";
            }
        }

        $fragment = $parsed['fragment'] ?? null;
        if (!empty($fragment)) {
            if (!$allowFragment) {
                return false;
            } else {
                $fragment = "#{$fragment}";
            }
        }

        return "{$scheme}://{$auth}{$host}{$path}{$query}{$fragment}";
    }

    /**
     * Проверка доменов.
     *
     * @param string $domain Домен.
     *
     * @return string|FALSE
     */
    final protected function checkDomain(string $domain)
    {
        if (filter_var($domain, FILTER_VALIDATE_IP)) {
            return $domain;
        } else {
            $domain = implode('.', array_map(function (string $part) {
                return idn_to_ascii($part, IDNA_DEFAULT, INTL_IDNA_VARIANT_UTS46);
            }, explode('.', $domain)));
            return filter_var($domain, FILTER_VALIDATE_DOMAIN);
        }
    }
}
