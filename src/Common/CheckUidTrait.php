<?php

namespace TextMedia\PlagiarismApi\Common;

/**
 * Примесь для проверки значения UID-а текста.
 * Вынесено, т.к. проверка может потребоваться и в Request, и в Response.
 */
trait CheckUidTrait
{
    /**
     * Проверка UID текста
     *
     * @param string $uid UID текста.
     *
     * @return string|FALSE
     */
    final public static function checkUid($uid)
    {
        return preg_match('#^[0-9a-f]{13}$#', $uid) ? $uid : false;
    }
}
