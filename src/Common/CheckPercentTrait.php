<?php

namespace TextMedia\PlagiarismApi\Common;

/**
 * Примесь для проверки процентных значений.
 */
trait CheckPercentTrait
{
    /**
     * Проверка процентных значений.
     *
     * @param mixed $value Процентное значение.
     *
     * @return string|FALSE
     */
    final protected function checkPercent($value)
    {
        return (is_scalar($value) && preg_match('#^\d+(.\d\d?)?$#', (string) $value))
            ? number_format((float) $value, 2, '.', '')
            : false;
    }
}
