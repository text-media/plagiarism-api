<?php

namespace TextMedia\PlagiarismApi\Common;

/**
 * Примесь, чтобы не копи-пастить методы для RawData (в Request/Response).
 */
trait RawDataTrait
{
    /** @var array Данные. */
    protected $rawData;

    /**
     * Преобразование в строку.
     *
     * @return string
     */
    final public function __toString(): string
    {
        static $options = JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES;
        return (string) json_encode($this->getRawData(), $options);
    }

    /**
     * Восстановление из массива (после var_export).
     *
     * @param array $data Данные для восстановления.
     *
     * @return self
     */
    final public static function __set_state(array $data)
    {
        return new static($data['rawData'] ?? null);
    }

    /**
     * Получение данных ответа.
     *
     * @return array
     */
    final public function getRawData(): array
    {
        return array_filter($this->rawData);
    }
}
