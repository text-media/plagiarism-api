<?php

namespace TextMedia\PlagiarismApi\Exception;

/**
 * Ошибки, возвращенные сервером API.
 */
final class ServerException extends AbstractException
{
    /** Ошибки: [код => описание]. */
    const ERRORS = [
        10 => 'Отсутствует проверяемый текст.',
        11 => 'Проверяемый текст пустой.',
        12 => 'Проверяемый текст слишком короткий.',
        13 => 'Проверяемый текст слишком большой. Разбейте текст на несколько частей.',
        20 => 'Отсутствует пользовательский ключ.',
        21 => 'Пользовательский ключ пустой.',
        40 => 'Ошибка доступа на сервере. Попробуйте позднее.',
        41 => 'Несуществующий пользовательский ключ.',
        42 => 'Нехватка символов на балансе.',
        43 => 'Ошибка при передаче параметров на сервере. Попробуйте позднее.',
        44 => 'Ошибка сервера. Попробуйте позднее.',
        45 => 'Ошибка сервера. Попробуйте позднее.',
        50 => 'Шинглов не найдено. Возможно текст слишком короткий.',
        60 => 'Отсутствует проверяемый uid текста.',
        61 => 'Uid текста пустой.',
        70 => 'Отсутствует пользовательский ключ.',
        71 => 'Пользовательский ключ пустой.',
        80 => 'Текущая пара ключ-uid отсутствует в базе.',
        81 => 'Текст ещё не проверен.',
        82 => 'Текст проверен с ошибками. Деньги будут возвращены.',
        83 => 'Ошибка сервера. Попробуйте позднее',
    ];

    /** Ошибка типа "wait". */
    const TYPE_WAIT = 'wait';

    /** Ошибка типа "skip". */
    const TYPE_SKIP = 'skip';

    /** Ошибка типа "stop". */
    const TYPE_STOP = 'stop';

    /** Сопоставление кодов и типов ошибок. */
    const TYPE_CODES = [
        self::TYPE_WAIT => [40, 43, 44, 45, 81, 83],
        self::TYPE_SKIP => [10, 11, 12, 13, 50, 60, 61, 82],
        self::TYPE_STOP => [20, 21, 41, 42, 70, 71, 80],
    ];

    /** @return string|NULL Тип ошибки. */
    public function getType()
    {
        foreach (self::TYPE_CODES as $type => $codes) {
            if (in_array($this->getCode(), $codes)) {
                return $type;
            }
        }
        return null;
    }

    /**
     * Проверка, требует ли данная ошибка просто подождать какое-то время,
     * прежде чем повторно выполнять вызвавший её запрос (сервер не доступен, текст еще не проверен и т.п.).
     *
     * @return boolean
     */
    public function requiresToWait(): bool
    {
        return ($this->getType() === self::TYPE_WAIT);
    }

    /**
     * Проверка, требует ли данная ошибка прекратить выполнять вызвавший её запрос
     * (текст пустой или слишком короткий/длиный, ошибка проверки и т.п.).
     *
     * @return boolean
     */
    public function requiresToSkip(): bool
    {
        return ($this->getType() === self::TYPE_SKIP);
    }

    /**
     * Проверка, требует ли данная ошибка прекратить выполнять любые запросы к серверу
     * (не хватает символов в пакетах, неправильный ключ API и т.п.).
     *
     * @return boolean
     */
    public function requiresToStop(): bool
    {
        return ($this->getType() === self::TYPE_STOP);
    }

    /**
     * Размер очереди (сколько текстов впереди).
     *
     * @return integer|NULL
     */
    public function getQueueSize()
    {
        return (
                is_array($this->debugInfo)
                && isset($this->debugInfo['queuetext'])
                && is_numeric($this->debugInfo['queuetext'])
            )
            ? (int) $this->debugInfo['queuetext']
            : null;
    }

    /**
     * Процент завершенности (на сколько процентов выполнена проверка).
     *
     * @return integer|NULL
     */
    public function getCompletionPercent()
    {
        return (
                is_array($this->debugInfo)
                && isset($this->debugInfo['queueproc'])
                && is_numeric($this->debugInfo['queueproc'])
            )
            ? (int) $this->debugInfo['queueproc']
            : null;
    }
}
