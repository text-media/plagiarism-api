<?php

namespace TextMedia\PlagiarismApi\Exception;

use Exception;
use Throwable;

/**
 * Исключения.
 */
abstract class AbstractException extends Exception
{
    /** Ошибки: [код => описание]. */
    const ERRORS = [];

    /** Текст для исключений с неизвестным кодом. */
    const UNKNOW_ERROR_CODE = 'Неизвестный код исключения: %s.';

    /** @var mixed Отладочная информация. */
    protected $debugInfo;

    /**
     * {@inheritDoc}
     *
     * @param array $params    OPTIONAL Параметры для подстановки в текст (по умолчанию пустой массив).
     * @param mixed $debugInfo OPTIONAL Отладочная информация (по умолчанию NULL).
     */
    final public function __construct(
        string $message = '',
        int $code = 0,
        Throwable $previous = null,
        array $params = [],
        $debugInfo = null
    ) {
        $this->debugInfo = $debugInfo;
        array_unshift($params, static::ERRORS[$code] ?? ($message ?: self::UNKNOW_ERROR_CODE));
        array_push($params, $code);
        $message = call_user_func_array('sprintf', $params);
        parent::__construct($message, $code, $previous);
    }

    /** @return mixed Отладочная информация. */
    final public function getDebugInfo()
    {
        return $this->debugInfo;
    }
}
