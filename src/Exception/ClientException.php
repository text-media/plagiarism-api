<?php

namespace TextMedia\PlagiarismApi\Exception;

use TextMedia\PlagiarismApi\Client;

/**
 * Исключения клиента.
 */
final class ClientException extends AbstractException
{
    /** Ошибки: [код => описание]. */
    const ERRORS = [
        1 => 'Неправильный формат API-ключа пользователя: \'%s\'.',
        2 => 'Ожидалась передача массива или объекта \'%s\'; передано - \'%s\'.',
        3 => 'Если API-ключ не передан в конструктор, он должен быть описан в переменной окружения "'
            . Client::ENTRY_POINT_ENV . '".',
    ];
}
